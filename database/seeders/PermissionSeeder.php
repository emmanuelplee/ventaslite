<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name'  => 'Categories_Index']);
        Permission::create(['name'  => 'Categories_Add']);
        Permission::create(['name'  => 'Categories_Edit']);
        Permission::create(['name'  => 'Categories_Delete']);

        Permission::create(['name'  => 'Products_Index']);
        Permission::create(['name'  => 'Products_Add']);
        Permission::create(['name'  => 'Products_Edit']);
        Permission::create(['name'  => 'Products_Delete']);

        Permission::create(['name'  => 'Sales_Index']);

        Permission::create(['name'  => 'Roles_Index']);
        Permission::create(['name'  => 'Roles_Add']);
        Permission::create(['name'  => 'Roles_Edit']);
        Permission::create(['name'  => 'Roles_Delete']);

        Permission::create(['name'  => 'Permissions_Index']);
        Permission::create(['name'  => 'Permissions_Add']);
        Permission::create(['name'  => 'Permissions_Edit']);
        Permission::create(['name'  => 'Permissions_Delete']);

        Permission::create(['name'  => 'Asignar_Index']);
        Permission::create(['name'  => 'Asignar_Add']);
        Permission::create(['name'  => 'Asignar_Edit']);
        Permission::create(['name'  => 'Asignar_Delete']);

        Permission::create(['name'  => 'Users_Index']);
        Permission::create(['name'  => 'Users_Add']);
        Permission::create(['name'  => 'Users_Edit']);
        Permission::create(['name'  => 'Users_Delete']);

        Permission::create(['name'  => 'Coins_Index']);
        Permission::create(['name'  => 'Coins_Add']);
        Permission::create(['name'  => 'Coins_Edit']);
        Permission::create(['name'  => 'Coins_Delete']);
    }
}
