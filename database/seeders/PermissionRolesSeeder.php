<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Asignar todos los permisos al role admin */
        for ($i=1; $i < 30; $i++) {
            DB::table('role_has_permissions')->insert([ 'permission_id' => $i, 'role_id' => 1 ]);
        }
        /* Asignar permisos a role 2 empleado */
        DB::table('role_has_permissions')->insert([ 'permission_id' => 1, 'role_id' => 2 ]);/* categorias */
        DB::table('role_has_permissions')->insert([ 'permission_id' => 5, 'role_id' => 2 ]);/* products */
        DB::table('role_has_permissions')->insert([ 'permission_id' => 6, 'role_id' => 2 ]);
        DB::table('role_has_permissions')->insert([ 'permission_id' => 7, 'role_id' => 2 ]);
        DB::table('role_has_permissions')->insert([ 'permission_id' => 8, 'role_id' => 2 ]);
        DB::table('role_has_permissions')->insert([ 'permission_id' => 9, 'role_id' => 2 ]);/* sales */
        DB::table('role_has_permissions')->insert([ 'permission_id' => 10, 'role_id' => 2 ]);/* roles */
        DB::table('role_has_permissions')->insert([ 'permission_id' => 14, 'role_id' => 2 ]);/* permission */
        DB::table('role_has_permissions')->insert([ 'permission_id' => 18, 'role_id' => 2 ]);/* asignar */
        DB::table('role_has_permissions')->insert([ 'permission_id' => 22, 'role_id' => 2 ]);/* users */
        DB::table('role_has_permissions')->insert([ 'permission_id' => 26, 'role_id' => 2 ]);/* coins */
    }
}
