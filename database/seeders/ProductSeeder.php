<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name'          => 'CURSOS Y LIVEWIRE',
            'description'   => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure, suscipit perferendis. Velit provident cumque recusandae.',
            'cost'          => 200,
            'price'         => 235,
            'barcode'       => '123',
            'stock'         => 1000,
            'alerts'        => 10,
            'image'         => 'cursos.png',
            'category_id'   => 1,
        ]);
        Product::create([
            'name'          => 'RUNNING NIKE',
            'description'   => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure, suscipit perferendis. Velit provident cumque recusandae.',
            'cost'          => 600,
            'price'         => 1500,
            'barcode'       => '1234',
            'stock'         => 1000,
            'alerts'        => 10,
            'image'         => 'tenis.png',
            'category_id'   => 2,
        ]);
        Product::create([
            'name'          => 'IPHONE 11',
            'description'   => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure, suscipit perferendis. Velit provident cumque recusandae.',
            'cost'          => 900,
            'price'         => 1400,
            'barcode'       => '12345',
            'stock'         => 1000,
            'alerts'        => 10,
            'image'         => 'iphone.png',
            'category_id'   => 3,
        ]);
        Product::create([
            'name'          => 'PC GAMMER',
            'description'   => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure, suscipit perferendis. Velit provident cumque recusandae.',
            'cost'          => 790,
            'price'         => 1350,
            'barcode'       => '123456',
            'stock'         => 1000,
            'alerts'        => 10,
            'image'         => 'pcgamer.png',
            'category_id'   => 4,
        ]);
    }
}
