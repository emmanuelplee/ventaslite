<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name'          => 'CURSOS',
            'description'   => 'En este curso crearemos una aplicación completamente desde cero, que nos permitirá llevar el control de productos.',
            'image'         => '1_course_test.jpg'
        ]);
        Category::create([
            'name'          => 'TENIS',
            'description'   => 'En este curso crearemos una aplicación completamente desde cero, que nos permitirá llevar el control de productos.',
            'image'         => '2_sneaker_test.jpg'
        ]);
        Category::create([
            'name'          => 'CELULARES',
            'description'   => 'En este curso crearemos una aplicación completamente desde cero, que nos permitirá llevar el control de productos.',
            'image'         => '3_mobile_test.jpg'
        ]);
        Category::create([
            'name'          => 'COMPUTADORAS',
            'description'   => 'En este curso crearemos una aplicación completamente desde cero, que nos permitirá llevar el control de productos.',
            'image'         => '4_computer_test.jpg'
        ]);
    }
}
