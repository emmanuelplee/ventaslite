<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'      => 'Admin',
            'phone'     => '2227343323',
            'email'     => 'admin@gmail.com',
            'status'    => 'ACTIVE',
            'password'  => bcrypt('admin1234'),
            'image'     => 'admin.png',
            'role_id'   => 1,
        ]);
        $user->syncRoles(1);
        $user = User::create([
            'name'      => 'Test',
            'phone'     => '2365147856',
            'email'     => 'test@gmail.com',
            'status'    => 'ACTIVE',
            'password'  => Hash::make('test'),
            'image'     => 'test.png',
            'role_id'   => 1,
        ]);
        $user->syncRoles(1);

        $user = User::create([
            'name'      => 'Fernando',
            'phone'     => '2236549875',
            'email'     => 'fernando@gmail.com',
            'status'    => 'ACTIVE',
            'password'  => Hash::make('fernando'),
            'image'     => 'fernado.png',
            'role_id'   => 2,
        ]);
        $user->syncRoles(2);
        // DB::table('users')->insert([
        //     'name'      => 'Fernando',
        //     'phone'     => '2236549875',
        //     'email'     => 'fernando@gmail.com',
        //     'status'    => 'ACTIVE',
        //     'password'  => Hash::make('fernando'),
        //     'image'     => 'fernado.png',
        //     'role_id'   => 2,
        // ]);

    }
}
