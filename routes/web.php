<?php

use App\Http\Controllers\ExportController;
use App\Http\Livewire\AsignarController;
use App\Http\Livewire\CashuotController;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\CategoriesController;
use App\Http\Livewire\CoinsController;
use App\Http\Livewire\PermisosController;
use App\Http\Livewire\PostController;
use App\Http\Livewire\ProductsController;
use App\Http\Livewire\ReportsController;
use App\Http\Livewire\RolesController;
use App\Http\Livewire\UsersController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::middleware(['auth'])->group(function () {
    // Route::get('/dashboard', ['as' => 'user.dashboard', 'uses' => 'DashboardController@index']);
    Route::get('categories', CategoriesController::class);
    Route::get('products',ProductsController::class);
    Route::get('coins', CoinsController::class);
    Route::get('pos', PostController::class);
    Route::get('roles', RolesController::class);
    Route::get('permisos', PermisosController::class);
    Route::get('asignar', AsignarController::class);
    Route::get('users', UsersController::class);
    Route::get('cashout',CashuotController::class);
    Route::get('reports',ReportsController::class);

});
// Reportes PDF
Route::get('report/pdf/{user}/{type}/{f1}/{f2}', [ExportController::class,'reportPDF']);
Route::get('report/pdf/{user}/{type}', [ExportController::class,'reportPDF']);
//Reportes Excel


