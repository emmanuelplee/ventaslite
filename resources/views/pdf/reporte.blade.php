<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte de Ventas</title>

    <link href="{{ asset('css/custom_pdf.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom_page.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
  <section class="header" style="top: -287px;">
  <!-- <section class="header"> -->
    <table cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td colspan="2" class="text-center" align="center">
          <span style="font-size: 25px; font-weight:bold;">R-al System</span>
        </td>
      </tr>
      <tr>
          <td width="30%" style="vertical-align: top; padding-top: 10px; position:relative">
            <img src="{{ asset('assets/img/logo_v1.jpg') }}" alt="" class="invoice-logo" width="150px">
          </td>
          <td width="70%" class="text-left text-company" style="vertical-align: top; padding-top: 10px;">
            @if ($reportType ==0)
                <span style="font-size: 16px;"><strong>Reportes de Ventas del Dia</strong></span>
            @else
                <span style="font-size: 16px;"><strong>Reportes de Ventas por Fechas</strong></span>
            @endif
            <br>
            @if ($reportType != 0)
                <span style="font-size: 16px;">
                    <strong>Fecha de consulta: {{$from_date}} al {{$to_date}}</strong>
                </span>
            @else
                <span style="font-size: 16px;">
                    @php
                     $date = \Carbon\Carbon::now()->format('d-m-Y')
                    @endphp
                    <strong>Fecha de consulta: {{$date}}</strong>
                </span>
            @endif
            <br>
            <span style="font-size: 14px;">Usuario: {{$user}}</span>
          </td>
      </tr>
    </table>
  </section>
  <section style="margin-top: -110px;">
    <table cellpadding="0" cellspacing="0" width="100%" class="table-items">
        <thead>
            <tr align="center">
                <th width="10%">FOLIO</th>
                <th width="15%">ESTATUS</th>
                <th width="15%">IMPORTE</th>
                <th width="10%">ITEMS</th>
                <th>USUARIO</th>
                <th width="18%">FECHA</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
                <tr align="center">
                    <th style="font-weight: 400;">{{$item->id}}</th>
                    <th style="font-weight: 400;">{{$item->status}}</th>
                    <th align="right" style="font-weight: 400;">${{number_format($item->total,2)}}</th>
                    <th style="font-weight: 400;">{{$item->items}}</th>
                    <th style="font-weight: 400;">{{$item->user}}</th>
                    @php
                        $created = \Carbon\Carbon::parse($item->created_at)
                    @endphp
                    <th style="font-weight: 400;">{{$created->format('d-m-Y')}}</th>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr align="center">
                <td colspan="1">
                </td>
                <td align="left">
                    <b>TOTALES:</b>
                </td>
                <td align="right">
                    <span style="font-weight: 600;">${{number_format($sumData,2)}}</span>
                </td>
                <td class="text-center">
                    <strong>${{$countData}}</strong>
                </td>
                <td colspan="2"></td>
            </tr>
        </tfoot>
    </table>
  </section>
  <br>
  <section class="footer">
    <table cellpadding="0" cellspacing="0" width="100%" class="table-items">
        <tr>
            <td width="20%">
                <span>R-al System V1</span>
            </td>
            <td width="60%" class="text-center">
                <span>emmanuelplee@gmail.com</span>
            </td>
            <td class="text-center" width="20%">
                <strong>Pagina:</strong><span class="pagenum"></span>
            </td>
        </tr>
    </table>
  </section>
</body>
</html>
