@include('common.modalHead')
  <div class="row">
    <!-- name -->
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="form-group">
        <label>Escribe el tipo de rol: </label>
        <div class="input-group">
          <div class="input-group-prepend input-group-text">
              <span class="far fa-edit"></span>
          </div>
          <input type="text"
              wire:model.lazy="name"
              class="form-control __focus_active"
              placeholder="Ingresa rol" maxlength="255">
        </div>
        @error('name')
            <span class="text-danger er">{{ $message }}</span>
        @enderror
      </div>
    </div>

  </div>

@include('common.modalFooter')
