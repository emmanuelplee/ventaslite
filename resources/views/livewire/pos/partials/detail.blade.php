<div class="connect-sorting">

  <div class="connect-sorting-content">
    <div class="card simple-title-task ui-sortable-handle">
      <div class="card-body _card_body_details">

        @if ($total > 0)
          <!-- <div class="table-responsive tblscroll"
              style="max-height: 650px; overflow: hidden"> -->
          <div class="table-responsive-md __table_responsive_xl">
            <table class="table table-md table-bordered table-striped">
              <thead>
                <tr class="text-center">
                  <th class="table-th">IMAGEN</th>
                  <th class="table-th">PRODUCTO</th>
                  <th class="table-th">PRECIO</th>
                  <th class="table-th" width="3%">CANT.</th>
                  <th class="table-th">IMPORTE</th>
                  <th class="table-th">ACCIONES</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($cart as $item)
                  <tr>
                    <!-- Imagen -->
                    <td class="text-center table-th">
                      @if (count($item->attributes) > 0)
                        @php
                          $path = public_path('/storage/products/' . $item->attributes[0]);
                          if (file_exists($path)) {
                              $image_ruta = 'products/' . $item->attributes[0];
                          }else {
                              $image_ruta = 'noimg.jpg';
                          }
                          if ($item->attributes[0] == null) {
                              $image_ruta = 'noimg.jpg';
                          }
                        @endphp
                        <span>
                          <img src="{{ asset('storage/' . $image_ruta) }}"
                                alt="img de producto" height="80" width="80" class="rounded __img-style">
                        </span>
                      @endif
                    </td>
                    <!-- descripcion -->
                    <td>
                      <p>{{ $item->name }}</p>
                    </td>
                    <!-- precio -->
                    <td class="text-center">
                        <div>${{ number_format($item->price,2) }}</div>
                    </td>
                    <!-- cantidad -->
                    <td>
                      <input type="number" id="r{{$item->id}}"
                        wire:change="updateQty({{$item->id}}, $('#r' + {{$item->id}}).val() )"
                        style="font-size: 1rem!important; width:100px"
                        class="for-control text-center cant_valida"
                        value="{{$item->quantity}}">
                    </td>
                    <!-- Importe -->
                    <td class="text-center">
                      <h6>
                        ${{ number_format(($item->price * $item->quantity) ,2)}}
                      </h6>
                    </td>
                    <!-- Acciones -->
                    <td class="text-center">
                      <button class="btn __mbmobile mb-1"
                        wire:click.prevent="decreaseQty({{ $item->id }})">
                        <i class="fas fa-minus"></i>
                      </button>
                      <button class="btn __mbmobile mb-1"
                        wire:click.prevent="increaseQty({{ $item->id }})">
                        <i class="fas fa-plus"></i>
                        </button>
                      <button class="btn __mbmobile mb-1"
                        onclick="Confirm('{{$item->id}}','removeItem',
                        '¿CONFIRMAS ELINIMAR EL REGISTRO?')">
                        <i class="fas fa-trash"></i>
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        @else
          <h5 class="text-center text-muted">Agrega productos a la venta</h5>
        @endif

        <div wire:loading.inline wire:targe="saveSale">
          <h4 class="text-danger text-center">Guardando Venta...</h4>
        </div>

      </div>
    </div>
  </div>

</div>
<script>

    /* metodo eliminar con sweetAlert2 */
  function Confirm(id, evenName, text) {
    console.log('id, eventName , text', id, evenName , text)
    swal({
      title: 'Estas Seguro',
      text: text,
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      cancelButtonColor: '#dc3545',
      confirmButtonColor: '#28a745',
      confirmButtonText: 'Eliminar',
            reverseButtons: true,
    }).then(function(result) {
      if (result.value) {
        window.livewire.emit(evenName, id)
                swal.close()
      }else if(result.dismiss === Swal.DismissReason.cancel) {
                swal({
                    title: 'Cancelado',
                    text: 'Tu registro esta a salvo :)',
                    type: 'error',
                    timer: 2000
                })
            }
    })
  };
</script>
