<div class="row mt-3">
  <div class="col-sm-12 col-md-12">
    <div class="connect-sorting">
      <h5 class="text-center mb-2">MONEDAS</h5>

      <div class="container">
        <div class="row">
          <!-- Monedas de la BD -->
          @foreach ($coins as $c)
            <div class="col-sm mt-2">
              <button class="btn btn-dark btn-block den"
                  wire:click.prevent="aCash({{ $c->value }})">
                  {{ $c->value > 0 ? '$'. number_format($c->value,2,'.','') : 'Exacto'}}
              </button>
            </div>
          @endforeach
        </div>
      </div>

      <div class="connect-sorting-content mt-4">
        <div class="card simple-title-task ui-sortable-handle">
          <div class="card-body">

            <div class="input-group input-group-md mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text input-gp hideonsm"
                    style="background: #3b3f5c; color:white"
                    >Efectivo F8
                </span>
              </div>

              <input type="number" id="cash"
                  wire:model="efectivo"
                  wire:keydown.enter="saveSale"
                  class="form-control text-center"
                  value="{{ $efectivo }}"
                  >

              <div class="input-group-append">
                <span class="input-group-text"
                    wire:click="$set('efectivo', 0)"
                    style="background: #3b3f5c; color:white">
                  <i class="fas fa-backspace fa-lg"></i>
                </span>
              </div>
            </div>

            <h4 class="text-muted">Cambio:
                ${{ $efectivo==0 ? number_format(($change=0),2) : number_format($change,2)}}
            </h4>

            <div class="mt-2">
              <div class="d-flex justify-content-between">
                @if ($total > 0)
                  <button class="btn btn-dark"
                      onclick="Confirm('','clearCart','¿SEGURO DE ELIMINAR EL CARRITO?')">
                      CANCELAR F4
                  </button>
                @endif
                @if ($efectivo >= $total && $total > 0)
                  <!-- <button class="btn btn-dark"
                      wire:click.prevent="saveSale">
                      GUARDAR F9
                  </button> -->
                  <button class="btn btn-dark"
                      onclick="saveSale('saveSale','¿SEGURO DE GUARDAR LA VENTA?')">
                      GUARDAR F9
                  </button>
                @endif
              </div>
              <!-- <div class="col-sm-6 col-md-12 col-lg-6">
              </div> -->
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>
