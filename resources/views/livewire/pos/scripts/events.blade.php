<script>

	document.addEventListener("DOMContentLoaded", function(event) {
        window.livewire.on('scan-ok', msg => {
            console.log('scan-ok',msg)
            noty(msg)
        })
        window.livewire.on('scan-notfound', msg => {
            console.log('scan-notfound',msg)
            noty(msg, 2)
        })
        window.livewire.on('no-stock', msg => {
            console.log('no-stock',msg)
            noty(msg, 2)
            location.reload();
        })
        window.livewire.on('no-stock-plus', msg => {
            console.log('no-stock-plus',msg)
            noty(msg, 2)
        })
        /* post metodo saveSale */
        window.livewire.on('sale-error', msg => {
            console.log('sale-error',msg)
            noty(msg, 2)
        })
        window.livewire.on('sale-ok', msg => {
            console.log('sale-ok',msg)
            noty(msg)
        })
        window.livewire.on('clearCash-ok', msg => {
            console.log('clearCash-ok',msg)
            // noty(msg)
        })
        window.livewire.on('print-ticket', saleId => {
            console.log('print-ticket',saleId)
            window.open("print://" + saleId + "_blank")
        })
    })

</script>
