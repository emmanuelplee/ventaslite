<script>

try {
    onScan.attachTo(document, {
        suffixKeyCodes: [13],
        onScan: function(barcode){
            console.log('barcode',barcode)
            window.livewire.emit('scan-code',barcode)
        },
        onScanError: function(e){
            console.log('error:', e);
        }
    })

    console.log('Scaner ready!');

} catch (error) {
    console.log('Error de lectura:', error);
}

</script>
