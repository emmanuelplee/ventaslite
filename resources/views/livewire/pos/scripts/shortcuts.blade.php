<script>
    var listener = new window.keypress.Listener();
    /* ejemplo */
    listener.simple_combo("shift s", function() {
    noty("Precionaste shift y s");
    });
    /* coins guardar venta */
    listener.simple_combo("f9", function(){
        window.livewire.emit('saveSale');
    })
    /* coins borrar efectivo  */
    listener.simple_combo("f8", function(){
        console.log('entra f8');
        document.getElementById('cash').value = '';
        document.getElementById('hiddenTotal').value = '';
        document.getElementById('cash').focus();
        //emit limpiar efectivo y cambio
        window.livewire.emit('clearCash')
    })

    listener.simple_combo("f4", function(){
        var total = parseFloat(document.getElementById('hiddenTotal').value);
        if (total > 0) {
            Confirm(0, 'clearCart','Segur@ de eliminar el carrito')
        }else{
            noty('Agrega productos a la venta');
        }
    })

</script>
