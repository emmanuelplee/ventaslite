<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $(".tblscroll").niceScroll({
            cursorcolor:"#515365",
            cursorwidth:"20px",
            background:"rgba(20,20,20,0.3)",
            cursorborder:"0px",
            cursorborderradius:3
        });

    })

    /* metodo eliminar con sweetAlert2 */
	function Confirm(id, evenName, text) {
		console.log('id, eventName , text', id, evenName , text)
		swal({
			title: 'Estas Seguro',
			text: text,
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'Cancelar',
			cancelButtonColor: '#dc3545',
			confirmButtonColor: '#28a745',
			confirmButtonText: 'Eliminar',
            reverseButtons: true,
		}).then(function(result) {
			if (result.value) {
				window.livewire.emit(evenName, id)
                swal.close()
			}else if(result.dismiss === Swal.DismissReason.cancel) {
                swal({
                    title: 'Cancelado',
                    text: 'Tu registro esta a salvo :)',
                    type: 'error',
                    timer: 2000
                })
            }
		})
	};
    /* metodo saveSale con sweetAlert2 */
	function saveSale(evenName, text) {
		console.log('eventName , text', evenName , text)
		swal({
			title: 'Guardar Venta',
			text: text,
			type: 'info',
			showCancelButton: true,
			cancelButtonText: 'Cerrar',
			cancelButtonColor: '#dc3545',
			confirmButtonColor: '#28a745',
			confirmButtonText: 'Guardar',
            reverseButtons: true,
		}).then(function(result) {
			if (result.value) {
				window.livewire.emit(evenName)
                swal.close()
			}
		})
	};
</script>
