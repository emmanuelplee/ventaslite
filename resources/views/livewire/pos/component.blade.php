<div>
  <style>

  </style>

  <div class="row layout-top-spacing">

    <div class="col-sm-12 col-md-7">
        <!-- Detail -->
        @include('livewire.pos.partials.detail')
    </div>

    <div class="col-sm-12 col-md-5">
        <!-- Total-->
        @include('livewire.pos.partials.total')

        <!-- Coins(denominations) -->
        @include('livewire.pos.partials.coins')

    </div>
  </div>

</div>

<script src="{{ asset('assets/js/keypress_v1.js')}}"></script>
<script src="{{ asset('assets/js/onscan.js')}}"></script>

@include('livewire.pos.scripts.events')
@include('livewire.pos.scripts.general')
@include('livewire.pos.scripts.shortcuts')
@include('livewire.pos.scripts.scan')
