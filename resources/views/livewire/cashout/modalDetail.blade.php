<div wire:ignore.self id="modal-details" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-dark">
          <h5 class="modal-title text-white">
            <b>Detalle de ventas</b>
          </h5>
          <button class="close" data-dismiss="modal" type="button" aria-label="close">
            <span class="text-white">&times;</span>
          </button>
      </div>

      <div class="modal-body">
        <div class="table-responsive-md">
          <table class="table table-md table-bordered table-striped">
            <thead>
              <tr class="text-center">
                  <th class="table-th">PRODUCTO</th>
                  <th class="table-th">CANT</th>
                  <th class="table-th">PRECIO</th>
                  <th class="table-th">IMPORTE</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($details as $item)
                <tr class="text-center">
                  <td><h6>{{$item->product}}</h6></td>
                  <td><h6>{{$item->quantity}}</h6></td>
                  <td><p>${{number_format($item->price,2)}}</p></td>
                  <td><p>${{number_format(($item->quantity * $item->price),2)}}</p></td>
                </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <td class="text-right"><h6 class="text-info">Totales:</h6></td>
                <td class="text-center">
                  @if ($details)
                    <h6 class="text-info">{{$details->sum('quantity')}}</h6>
                  @endif
                </td>

                @if ($details)
                  @php
                    $mytotal=0;
                  @endphp
                  @foreach ($details as $d)
                    @php
                      $mytotal += $d->quantity * $d->price;
                    @endphp
                  @endforeach
                  <td></td>
                  <td class="text-center">
                    <h6 class="text-info">${{number_format($mytotal,2)}}</h6>
                  </td>
                @endif
              </tr>
            </tfoot>
          </table>
        </div>
      </div>

    </div>
  </div>
</div>
