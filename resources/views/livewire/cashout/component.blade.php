<div class="row sales layout-top-spacing">
  <div class="col-sm-12">
    <div class="widget widget-chart-one">
      <!-- Begin Titulo de vista -->
      <div class="widget-heading">
        <h4 class="car-title text-center"><b>Corte de caja</b>
        </h4>
      </div>
      <!-- End Titulo de vista -->
      <!-- Begin inputs y acciones  -->
      <div class="widget-content">
        <div class="row text-center">
            <!-- Usuario -->
          <div class="col-sm-12 col-md-3">
            <div class="form-group">
              <label>Usuario</label>
              <select wire:model="user_id" class="form-control">
                  <option value="0">== Elegir Usuario ==</option>
                  @foreach ($users as $user)
                      <option value="{{$user->id}}">{{$user->name}}</option>
                  @endforeach
              </select>
              @error('user_id')
                <span class="text-danger er">{{ $message }}</span>
              @enderror
            </div>
          </div>
          <!-- Fecha inicial -->
          <div class="col-sm-12 col-md-3">
            <div class="form-group">
              <label>Fecha Inicial</label>
              <input type="date" wire:model.lazy="from_date" class="form-control">
              @error('from_date')
                <span class="text-danger er">{{ $message }}</span>
              @enderror
            </div>
          </div>
          <!-- fecha final -->
          <div class="col-sm-12 col-md-3">
            <div class="form-group">
              <label>Fecha final</label>
              <input type="date" wire:model.lazy="to_date" class="form-control">
              @error('to_date')
                <span class="text-danger er">{{ $message }}</span>
              @enderror
            </div>
          </div>
          <!-- Boton -->
          <div class="col-sm-12 col-md-3 align-self-center">
            <div class="d-flex justify-content-around">
              @if ($user_id > 0 && $from_date != null && $to_date != null)
                <button wire:click.prevent="consultar()"
                    type="button" class="btn __mbmobile __agregar text-white">Consultar
                </button>
              @endif
              @if ($total > 0)
                <button wire:click.prevent="print()"
                    type="button" class="btn __mbmobile __agregar text-white">Imprimir
                </button>
            @endif
            </div>
          </div>
        </div>
      </div>
      <!-- Begin total y tabla -->
      <div class="row mt-50">
          <div class="col-sm-12 col-md-4 __mbmobile">
              <div class="connect-sorting bg-dark">
                <h5 class="text-white">Ventas Totales: ${{number_format($total,2)}}</h5>
                <h5 class="text-white">Articulos: ${{$items}}</h5>
              </div>
          </div>
          <div class="col-sm-12 col-md-8">
            <div class="table-responsive-md">
            <table class="table table-md table-bordered table-striped">
              <thead>
                <tr class="text-center">
                    <th class="table-th">folio</th>
                    <th class="table-th">total</th>
                    <th class="table-th">items</th>
                    <th class="table-th">FECHA</th>
                    <th class="table-th">ACCIONES</th>
                </tr>
              </thead>
              <tbody>
                  @if ($total <= 0)
                  <tr>
                      <td colspan="5">
                          <h6 class="text-center">No hay ventas en la fecha seleccionada</h6>
                      </td>
                  </tr>
                  @endif
                @foreach ($sales as $item)
                  <tr class="text-center">
                    <td>
                        <h6>{{$item->id}}</h6>
                    </td>
                    <td>
                        <p>{{number_format($item->total,2)}}</p>
                    </td>
                    <td>
                        <p>{{$item->items}}</p>
                    </td>
                    <td>
                        <p>{{$item->created_at}}</p>
                    </td>
                    <td>
                        <button wire:click.prevent="viewDetails({{$item}})"
                            class="btn __mbmobile btn-sm">
                            <i class="fas fa-list"></i>
                        </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
                <!-- Pagination -->
                <div style="display: flex;justify-content: center;">
                <!-- {{'$sales->links()'}} -->
            </div>
            </div>
          </div>
      </div>
    </div>
  </div>
    <!-- Modal id="#theModal" -->
  @include('livewire.cashout.modalDetail')
</div>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    window.livewire.on('item-modal-show', msg => {
      console.log('Emit item-modal-show msg:', msg)
      $('#modal-details').modal('show');
    });
    window.livewire.on('consulta-ok',msg => {
        noty(msg)
    })
    window.livewire.on('consulta-error',msg => {
        noty(msg,2)
    })
  });

</script>
