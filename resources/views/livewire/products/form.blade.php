@include('common.modalHead')
<div class="row">
    <!-- NAME -->
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label>Nombre producto: </label>
            <div class="input-group">
                <!-- <div class="input-group-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div> -->
                <input type="text"
                    wire:model.lazy="name"
                    class="form-control __focus_active"
                    autofocus
                    placeholder="Nombre producto">
            </div>
            @error('name')
                <span class="text-danger er">{{ $message }}</span>
            @enderror
        </div>
    </div>
    <!-- BARCODE -->
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label>Código de barras:</label>
            <div class="input-group">
                <!-- <div class="input-group-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div> -->
                <input type="text"
                    wire:model.lazy="barcode"
                    class="form-control"
                    placeholder="Código de barras">
            </div>
            @error('barcode')
                <span class="text-danger er">{{ $message }}</span>
            @enderror
        </div>
    </div>
    <!-- DESCRIPTION -->
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
            <label>Escribe una breve descripción del producto: </label>
            <div class="input-group">
                <!-- <div class="input-group-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div> -->
                <textarea wire:model.lazy="description"
                    class="form-control"
                    placeholder="Descripción"
                    rows="3"></textarea>
            </div>
            @error('description')
                <span class="text-danger er">{{ $message }}</span>
            @enderror
        </div>
    </div>
    <!-- COST -->
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label>Costo aquirido: </label>
            <div class="input-group">
                <!-- <div class="input-group-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div> -->
                <input type="number"
                    pattern="[0-9]+([\.,][0-9]+)?"
                    wire:model.lazy="cost"
                    class="form-control"
                    placeholder="Ingresa costo">
            </div>
            @error('cost')
                <span class="text-danger er">{{ $message }}</span>
            @enderror
        </div>
    </div>
    <!-- PRECI -->
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label>Precio venta: </label>
            <div class="input-group">
                <!-- <div class="input-gruop-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div> -->
                <input type="number"
                    pattern="[0-9]+([\.,][0-9]+)?"
                    wire:model.lazy="price"
                    class="form-control"
                    placeholder="Ingresa precio">
            </div>
            @error('price')
                <span class="text-danger er"></span>
            @enderror
        </div>
    </div>
    <!-- STOCK -->
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label>Stock:</label>
            <div class="input-group bs-tooltip" data-placement="top" title="Cantidad de existencias">
                <!-- <div class="input-gruop-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div> -->
                <input type="number"
                wire:model.lazy="stock"
                class="form-control"
                placeholder="Ingresa cantidad">
            </div>
            @error('stock')
                <span class="text-danger er">{{ $message }}</span>
            @enderror
        </div>
    </div>
    <!-- ALERTS -->
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label>Inv. Minimo: </label>
            <div class="input-group">
                <!-- <div class="input-gruop-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div> -->
                <input type="number"
                wire:model.lazy="alerts"
                class="form-control"
                placeholder="Ingresa cantidad minima">
            </div>
            @error('alerts')
                <span class="text-danger er">{{ $message }}</span>
            @enderror
        </div>
    </div>
    <!-- CATEGORIES -->
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label>Categoria: </label>
            <div class="input-group">
                <!-- <div class="input-group-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div> -->
                <select class="form-control"
                    wire:model.lazy="category_id">
                    <option value="ELEGIR">Elegir categoria</option>
                    <!-- Tabla de category -->
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
                @error('category_id')
                    <span class="text-danger er">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>
    <!-- IMAGE -->
    <div class="col-sm-12 col-md-12 col-lg-12">
        <label for="image">Agrega una imagen para el producto: </label>
        @if ($image && !is_string($image))
        <img src="{{ $image->temporaryUrl() }}" height="150" width="170" class="rounded __img-style">
        @endif
        <div class="form-group custom-file">
            <input type="file" class="custom-file-input form-control" wire:model.lazy="image" accept="image/jpeg,image/png,image/git">
            <label class="custom-file-label">Nombre: {{$image}}</label>
            @error('image')
            <span class="text-danger er">{{$message}}</span>
            @enderror
        </div>
        @if ($selected_id > 0 && is_string($image))
            <div class="text-center mb-3">
                <img src="{{asset('storage/'.$image)}}" alt="imagen ejemplo" height="150" width="170" class="rounded __img-style">
            </div>
        @endif
    </div>

</div>
@include('common.modalFooter')
