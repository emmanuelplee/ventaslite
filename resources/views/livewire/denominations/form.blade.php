@include('common.modalHead')
<div class="row">

    <!-- type -->
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label>Eligir tipo:</label>
            <div class="input-group">
                <div class="input-group-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div>
                <select wire:model="type" class="form-control __focus_active">
                    <option value="ELEGIR">Elegir tipo</option>
                    <option value="BILLETE">BILLETE</option>
                    <option value="MONEDA">MONEDA</option>
                    <option value="OTRO">OTRO</option>
                </select>
            </div>
            @error('type')
                <span class="text-danger er">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <!-- value -->
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label>Escribe el valor: </label>
            <div class="input-group">
                <div class="input-group-prepend input-group-text">
                    <span class="far fa-edit"></span>
                </div>
                <input type="number"
                    wire:model.lazy="value"
                    class="form-control"
                    placeholder="Ingresa valor">
            </div>
            @error('value')
                <span class="text-danger er">{{ $message }}</span>
            @enderror
        </div>
    </div>
    <!-- image -->
    <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
        <label for="image">Agrega una imagen para el valor:</label>
        @if ($image && !is_string($image))
        <img src="{{ $image->temporaryUrl() }}" height="150" width="170" class="rounded __img-style">
        @endif
        <div class="form-group custom-file">
            <input type="file" class="custom-file-input form-control" wire:model.lazy="image" accept="image/jpeg,image/png,image/git">
            <label class="custom-file-label">Nombre: {{$image}}</label>
            @error('image')
            <span class="text-danger er">{{$message}}</span>
            @enderror
        </div>
        @if ($selected_id > 0 && is_string($image))
            <div class="text-center mb-3">
                <img src="{{asset('storage/'.$image)}}" alt="imagen ejemplo" height="150" width="170" class="rounded __img-style">
            </div>
        @endif
    </div>

</div>

@include('common.modalFooter')
