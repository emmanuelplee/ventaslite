<div class="row sales layout-top-spacing">
  <div class="col-sm-12">
    <div class="widget widget-chart-one">
      <!-- Begin Titulo de vista y accion agregar -->
      <div class="widget-heading">
        <h5 class="car-title">
          <b>{{ $componentName }} | {{ $pageTitle }}</b>
        </h5>
        @can('Coins_Add')
          <ul class="tabs tab-pills">
            <li>
              <!-- Botton de Agregar -->
              <a href="javascript:void(0)" class="tabmenu p-2 __agregarCoin"
                data-toggle="modal" data-target="#theModal">Agregar
              </a>
            </li>
          </ul>
         @endcan
      </div>
      <!-- End Titulo de vista y accion agregar -->
      <!-- Begin Buscador -->
      @include('common.searchbox')
      <!-- End Buscador -->
      <!-- Begin tabla  -->
      <div class="widget-content">
        <!-- <pre>{{ 'print_r($data)'}} -->
        <div class="table-responsive-md">
          <table class="table table-md table-bordered table-striped">
            <thead>
              <tr class="text-center">
                <th class="table-th">#</th>
                <th class="table-th">TIPO</th>
                <th class="table-th">VALOR</th>
                <th class="table-th">IMAGEN</th>
                @canany(['Coins_Edit','Coins_Delete'])
                  <th class="table-th">ACCIONES</th>
                @endcanany
              </tr>
            </thead>
            <tbody>
              @foreach ($data as $item)
                <tr class="text-center">
                  <td style="width: 50px;">
                      <h6>{{ $item->id}}</h6>
                  </td>
                  <td>
                      <p>{{ $item->type }}</p>
                  </td>
                  </td>
                  <td>
                      <p>$ {{ number_format($item->value,2) }}</p>
                  </td>
                  <td>
                    <span>
                      <!-- Imagen Accessor Modelo Product -->
                      <img src="{{asset('storage/'.$item->image_accessor)}}"
                        alt="imagen ejemplo" height="75" width="80" class="rounded __img-style">
                    </span>
                  </td>
                  <td>
                    @can('Coins_Edit')
                      <!-- Botton de Editar-->
                    <a href="javascript:void(0)"
                        wire:click.prevent="edit({{$item->id}})"
                        class="btn __mtmobile mtmobile bs-tooltip mb-1"
                        data-placement="top"
                        title="Editar">
                      <i class="fas fa-pencil-alt"></i>
                    </a>
                    @endcan
                    @can('Coins_Delete')
                    <!-- Botton de Elimimar -->
                    <a href="javascript:void(0)"
                        onclick="Confirm('{{$item->id}}')"
                        class="btn __mtmobile mtmobile bs-tooltip mb-1"
                        data-placement="top"
                        title="Eliminar">
                      <i class="fas fa-trash"></i>
                    </a>
                    @endcan
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
            <div style="display: flex;justify-content: center;">
            {{$data->links()}}
          </div>
        </div>
      </div>
    </div>
  </div>
    <!-- Modal id="#theModal" -->
  @include('livewire.denominations.form')
</div>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    /* CoinsController metodo edit */
    window.livewire.on('item-modal-show', msg => {
      console.log('Emit item-modal-show msg:', msg)
      $('#theModal').modal('show');
    });
    /* CoinsController metodo store */
    window.livewire.on('item-added', msg => {
      console.log('Emit item-added msg:', msg)
      $('#theModal').modal('hide');
      swal({
        title: 'Agregado!',
        text: 'El registro ha sido agregado',
        type: 'success',
        timer: 2000
      });
    });
    /* CoinsController metodo update */
    window.livewire.on('item-updated', msg => {
      console.log('Emit item-updated msg:', msg)
      $('#theModal').modal('hide');
      swal({
        title: 'Actualizado!',
        text: 'El registro ha sido actualizado',
        type: 'success',
        timer: 2000
      });
    });
        /* CoinsController metodo delete */
    window.livewire.on('item-deleted', msg => {
      console.log('Emit item-deleted msg:', msg)
      $('#theModal').modal('hide');
      swal({
        title: 'Eliminado!',
        text: 'El registro ha sido eliminado',
        type: 'success',
        timer: 2000
      });
    });

    /* ?? Modal hide show livewire ?? */
    window.livewire.on('modal-show', msg => {
      console.log('Emit modal-show msg:', msg)
      $('#theModal').modal('show');
    });
    window.livewire.on('modal-hide', msg => {
      console.log('Emit modal-hide msg:', msg)
      $('#theModal').modal('hide');
    });

    /* Modal borrar Errors del form */
    $('#theModal').on('hidden.bs.modal', function(e) {
      $('.er').css('display','none');
    });
    /* Modal focus input del form */
    $('#theModal').on('hidden.bs.modal', msg => {
      $('.__focus_active').focus();
    });
  });

  /* metodo eliminar con sweetAlert */
  function Confirm(id) {
    console.log('id,', id)
    swal({
      title: 'Estas Seguro',
      text: '¿Confima para eliminar el registro?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      cancelButtonColor: '#dc3545',
      confirmButtonColor: '#28a745',
      confirmButtonText: 'Eliminar',
            reverseButtons: true,
    }).then(function(result) {
      if (result.value) {
        window.livewire.emit('deleteRow', id)
                swal.close()
      }else if(result.dismiss === Swal.DismissReason.cancel) {
                swal({
                    title: 'Cancelado',
                    text: 'Tu reguistro esta a salvo :)',
                    type: 'error',
                    timer: 2000
                })
            }
    })
  };
</script>
