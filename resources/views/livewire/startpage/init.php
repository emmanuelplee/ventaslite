<div class="row sales layout-top-spacing">
  <div class="col-sm-12">
    <div class="widget widget-chart-one">
      <!-- Begin Titulo de vista y accion agregar -->
      <div class="widget-heading">
        <h4 class="car-title">
          <b>{{ '$componentName' }} | {{ '$pageTitle' }}</b>
        </h4>
        <ul class="tabs tab-pills">
          <li>
            <!-- Botton de Agregar -->
            <a href="javascript:void(0)"
              class="tabmenu __agregar p-2"
              data-toggle="modal"
              data-target="#theModal"
              style="font-size: 0.95rem;">
              Agregar</a>
          </li>
        </ul>
      </div>
      <!-- End Titulo de vista y accion agregar -->
      <!-- Begin Buscador -->
      @include('common.searchbox')
      <!-- End Buscador -->
      <!-- Begin tabla  -->
      <div class="widget-content">
        <!-- <pre>{{ 'print_r($data)'}} -->
        <div class="table-responsive">
          <table class="table table-md table-bordered table-striped">
            <thead>
              <tr class="text-center">
                <th class="table-th">#</th>
                <th class="table-th">DESCRIPCION</th>
                <th class="table-th">IMAGEN</th>
                <th class="table-th">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              <!-- foreach ($data as $item) -->
                <tr class="text-center">
                  <td>
                      <h6>{{ '$item->id'}}</h6>
                  </td>
                  <td class="text-left">
                      <p>{{ '$item->descrption' }}</p>
                  </td>
                  <td>
                    <span>
                      <!-- Imagen Accessor Modelo Product -->
                      <img src="{{asset('storage/products/'.'$item->image_accessor')}}"
                          alt="imagen ejemplo" height="70" width="90" class="rounded">
                    </span>
                  </td>
                  <td>
                    <!-- Botton de Editar-->
                    <a href="javascript:void(0)"
                        wire:click.prevent="edit({{ '$item->id' }})"
                        class="btn __mtmobile bs-tooltip mb-1"
                        data-placement="top"
                        title="Editar">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                      <!-- Botton de Elinimar -->
                    <a href="javascript:void(0)"
                        onclick="Confirm('{ $item->id }')"
                        class="btn __mtmobile bs-tooltip mb-1"
                        data-placement="top"
                        title="Eliminar">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
              <!-- endforeach -->
            </tbody>
          </table>
            <!-- Pagination -->
            pagination
            <div style="display: flex;justify-content: center;">
            <!-- {{'$data->links()'}} -->
          </div>
        </div>
      </div>
    </div>
  </div>
  Include Form
    <!-- Modal id="#theModal" -->
  <!-- include('livewire.products.form') -->
</div>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    /* CategoriosController metodo store */
    window.livewire.on('item-added', msg => {
      console.log('Emit item-added msg:', msg)
      $('#theModal').modal('hide');
      swal({
        title: 'Agregado!',
        text: 'El registro ha sido agregado',
        type: 'success',
        timer: 2000
      });
    });
    /* CategoriosController metodo edit */
    window.livewire.on('item-modal-show', msg => {
      console.log('Emit item-modal-show msg:', msg)
      $('#theModal').modal('show');
    });
    /* CategoriosController metodo update */
    window.livewire.on('item-updated', msg => {
      console.log('Emit item-updated msg:', msg)
      $('#theModal').modal('hide');
      swal({
        title: 'Actualizado!',
        text: 'El registro ha sido actualizado',
        type: 'success',
        timer: 2000
      });
    });
        /* CategoriosController metodo delete */
    window.livewire.on('item-deleted', msg => {
      console.log('Emit item-deleted msg:', msg)
      $('#theModal').modal('hide');
      swal({
        title: 'Elinimado!',
        text: 'El registro ha sido eliminado',
        type: 'success',
        timer: 2000
      });
    });

    /* Modal hide */
    window.livewire.on('modal-hide', msg => {
      console.log('Emit modal-hide msg:', msg)
      $('#theModal').modal('hide');
    });
    /* Modal borrar Errors del form */
    $('#theModal').on('hidden.bs.modal', function(e) {
      $('.er').css('display','none');
    });
    /* Modal focus input del form */
    $('#theModal').on('shown.bs.modal', msg => {
      $('.__focus_active').focus();
    });
  });

  /* metodo eliminar con sweetAlert */
  function Confirm(id) {
    console.log('id', id)
    swal({
      title: 'Estas Seguro',
      text: '¿Confima para eliminar el registro?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      cancelButtonColor: '#dc3545',
      confirmButtonColor: '#28a745',
      confirmButtonText: 'Eliminar',
            reverseButtons: true,
    }).then(function(result) {
      if (result.value) {
        window.livewire.emit('deleteRow', id)
        swal.close()
      }else if(result.dismiss === Swal.DismissReason.cancel) {
                swal({
                    title: 'Cancelado',
                    text: 'Tu reguistro esta a salvo :)',
                    type: 'error',
                    timer: 2000
                })
            }
    })
  };
</script>
