<div class="row sales layout-top-spacing">
  <div class="col-sm-12">
    <div class="widget widget-chart-one">
      <!-- Begin Titulo de vista y accion agregar -->
      <div class="widget-heading">
        <h4 class="car-title">
          <b>{{$componentName}}</b>
        </h4>
        </ul>
      </div>
      <!-- End Titulo de vista y accion agregar -->
      <!-- Begin tabla  -->
      <div class="widget-content">
        <div class="form-inline">
          <div class="for-group mr-5 mb-1">
            <select wire:model="role" class="form-control">
              <option value="ELEGIR">== Selecciona el role ==</option>
              @foreach ($roles as $role)
                <option value="{{$role->id}}">{{$role->name}}</option>
              @endforeach
            </select>
          </div>
          <button wire:click.prevent="syncAll()" type="button"
              class="btn __agregar __mbmobile inblock mr-5 text-white">Syncronizar Todos
          </button>
          <button onclick="Revocar()" type="button"
              class="btn __agregar __mbmobile mr-5 text-white">Revocar Todos
          </button>
        </div>
        <div class="row mt-3">
          <div class="col-sm-12">
            <div class="table-responsive-md">
              <table class="table table-md table-bordered table-striped">
                <thead>
                  <tr class="text-center">
                    <th class="table-th">ID</th>
                    <th class="table-th">PERMISO</th>
                    <th class="table-th">ROLES CON EL PERMISO</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($permisos as $permiso)
                    <tr class="text-center">
                      <td>
                        <h6>{{$permiso->id}}</h6>
                      </td>
                      <td>
                        <div class="n-check">
                          <label class="new-control new-checkbox checkbox-primary">
                            <input type="checkbox" id="p{{$permiso->id}}"
                              wire:change="syncPermiso($('#p'+{{$permiso->id}}).is(':checked'),'{{$permiso->name}}')"
                              value="{{$permiso->id}}" class="new-control-input"
                              {{ $permiso->checked == 1 ? 'checked' : ''}}>
                              <span class="new-control-indicator"></span>
                              <h6>{{$permiso->name}}</h6>
                          </label>
                        </div>
                      </td>
                      <td>
                        <h6>{{ \App\Models\User::permission($permiso->name)->count() }}</h6>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
                <!-- Pagination -->
                <div style="display: flex;justify-content: center;">
                {{$permisos->links()}}
              </div>
            </div>
          </di>
        </di>
      </div>
    </div>
  </div>
</div>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {

    window.livewire.on('sync-error', msg => {
      noty(msg,2)
    });
    window.livewire.on('permi', msg => {
      noty(msg)
    });
    window.livewire.on('syncall', msg => {
      noty(msg)
    });
    window.livewire.on('removeall', msg => {
      noty(msg)
    });

  });

  /* metodo eliminar con sweetAlert */
  function Revocar() {
    swal({
      title: 'Estas Seguro',
      text: '¿Confima revocar todos los permisos?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      cancelButtonColor: '#dc3545',
      confirmButtonColor: '#28a745',
      confirmButtonText: 'Aceptar',
      reverseButtons: true,
    }).then(function(result) {
      if (result.value) {
        window.livewire.emit('revokeAll')
        swal.close()
      }else if(result.dismiss === Swal.DismissReason.cancel) {
        swal({
            title: 'Cancelado',
            text: 'Tu permiso esta a salvo :)',
            type: 'error',
            timer: 2000
        })
      }
    })
  };
</script>
