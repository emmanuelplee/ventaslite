<div class="row sales layout-top-spacing">
  <div class="col-sm-12">
    <div class="widget widget-chart-one">
      <!-- Begin Titulo de vista y accion agregar -->
      <div class="widget-heading">
        <h5 class="car-title">
            <b>{{ $componentName }} | {{ $pageTitle }}</b>
        </h5>
        @can('Categories_Add')
          <ul class="tabs tab-pills">
            <li>
              <!-- Botton de Agregar -->
              <a href="javascript:void(0)" class="tabmenu p-2 __agregarC"
                data-toggle="modal" data-target="#theModal">Agregar
              </a>
            </li>
          </ul>
        @endcan
      </div>
      <!-- End Titulo de vista y accion agregar -->
      <!-- Begin Buscador Comun-->
      @include('common.searchbox')
      <!-- End Buscador -->
      <!-- include('livewire.category.carousel') -->
      <!-- Begin tabla  -->
      <div class="widget-content">
        <!-- <pre>{{ print_r($data) }} -->
        <div class="table-responsive-md">
          <table class="table table-md table-bordered table-striped">
            <thead>
              <tr class="text-center">
                <th class="table-th">#</th>
                <th class="table-th">IMAGEN</th>
                <th class="table-th">NOMBRE</th>
                <th class="table-th">DESCRIPCION</th>
                <th class="table-th">PRODUCTOS</th>
                @canany(['Categories_Edit','Categories_Delete'])
                  <th class="table-th">ACCIONES</th>
                @endcanany
              </tr>
            </thead>
            <tbody>
              @foreach($data as $item)
              <tr class="text-center">
                <td>
                  <h6>{{ $item->id }}</h6>
                </td>
                <td  style="width: 100px;">
                    <span>
                    <!-- Imagen Accessor Modelo category -->
                      <img src="{{ asset('storage/'.$item->image_accessor) }}"
                        alt="imagen ejemplo" height="75" width="80"
                        class="rounded __img-style">
                    </span>
                </td>
                <td>
                  <p>{{ $item->name }}</p>
                </td>
                <td class="text-left">
                  <p>{{ $item->description }}</p>
                </td>
                <td>
                  <!-- Metodo es relacion con productos -->
                  @if ($item->products->count() <= 0 )
                    <p class="__borde_alert_p" style="border-color: #c9341c;">
                        {{ $item->products->count() }}
                    </p>
                    @else
                    <p>
                        {{ $item->products->count() }}
                    </p>
                  @endif
                </td>
                <td>
                  @can('Categories_Edit')
                    <a href="javascript:void(0)" wire:click="edit({{ $item->id }})" class="btn __mtmobile bs-tooltip mb-1" data-placement="top" title="Editar">
                      <i class="fas fa-pencil-alt"></i>
                    </a>
                  @endcan
                  @can('Categories_Delete')
                    <a href="javascript:void(0)"
                      onclick="Confirm('{{ $item->id }}','{{ $item->products->count() }}')"
                      class="btn  __mtmobile bs-tooltip mb-1" data-placement="top"
                      title="Eliminar">
                      <i class="fas fa-trash"></i>
                    </a>
                  @endcan
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
            <div style="display: flex;justify-content: center;">
                {{ $data->links() }}
            </div>
        </div>

      </div>
      <!-- End tabla -->
    </div>
  </div>
  <!-- Modal id="#theModal" -->
  @include('livewire.category.form')
</div>

@include('livewire.category.scripts')
