<script>
	document.addEventListener("DOMContentLoaded", function(event) {
		/* CategoriesController metodo edit */
		window.livewire.on('modal-show-item', msg => {
			console.log('Emit modal-show-item msg:', msg)
			$('#theModal').modal('show');
		});
		/* CategoriesController metodo store */
		window.livewire.on('item-added', msg => {
			console.log('Emit item-added msg:', msg)
			$('#theModal').modal('hide');
			swal({
				title: 'Agregado!',
				text: 'El registro ha sido agregado',
				type: 'success',
				timer: 2000
			});
		});
		/* CategoriesController metodo update */
		window.livewire.on('item-update', msg => {
			console.log('Emit item-update msg:', msg)
			$('#theModal').modal('hide');
			swal({
				title: 'Actualizado!',
				text: 'El registro ha sido actualizado',
				type: 'success',
				timer: 2000
			});
		});
        /* CategoriesController metodo delete */
		window.livewire.on('item-deleted', msg => {
			console.log('Emit item-deleted msg:', msg)
			$('#theModal').modal('hide');
			swal({
				title: 'Eliminado!',
				text: 'El registro ha sido eliminado',
				type: 'success',
				timer: 2000
			});
		});

        /* Modal borrar Errors del form */
        $('#theModal').on('hidden.bs.modal', function(e) {
			$('.er').css('display','none');
		});
        /* Modal focus input del form */
        $('#theModal').on('shown.bs.modal', function(e) {
			$('.__focus_active').focus();
		});
	});

	/* metodo eliminar con sweetAlert2 */
	function Confirm(id, produts) {
		console.log('id, productos', id, produts)
        if (produts > 0) {
            swal({
                title:'NO SE PUEDE ELIMINAR',
                text: 'POR QUE LA CATEGORIA TIENE PRODUCCTOS RELACIONADOS',
                type: 'info',
                confirmButtonText: 'OK'
            })
            return;
        }
		swal({
			title: 'Estas Seguro',
			text: '¿Confima para eliminar el registro?',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'Cancelar',
			cancelButtonColor: '#dc3545',
			confirmButtonColor: '#28a745',
			confirmButtonText: 'Eliminar',
            reverseButtons: true,
		}).then(function(result) {
			if (result.value) {
				window.livewire.emit('deleteRow', id)
                swal.close()
			}else if(result.dismiss === Swal.DismissReason.cancel) {
                swal({
                    title: 'Cancelado',
                    text: 'Tu reguistro esta a salvo :)',
                    type: 'error',
                    timer: 2000
                })
            }
		})
	};
</script>
