@include('common.modalHead')
<div class="row">

  <div class="col-sm-12">
    <label for="name">Ingresa nombre de la categoria:</label>
    <div class="input-group">
      <!-- <div class="input-group-prepend">
        <span class="input-group-text">
          <span class="far fa-edit"></span>
        </span>
      </div> -->
      <input type="text"
        wire:model.lazy="name"
        class="form-control __focus_active"
        placeholder="Nombre categoria"
        autofocus>
    </div>
    @error('name')
      <span class="text-danger er">{{$message}}</span>
    @enderror
  </div>

  <div class="col-sm-12 mt-3">
    <label for="name">Escribe una breve descripción del la categoria:</label>
    <div class="input-group">
      <!-- <div class="input-group-prepend">
        <span class="input-group-text">
          <span class="far fa-edit"></span>
        </span>
      </div> -->
      <textarea wire:model.lazy="description" class="form-control" placeholder="Descripcion" rows="3"></textarea>
    </div>
    @error('description')
      <span class="text-danger er">{{$message}}</span>
    @enderror
  </div>

    <div class="col-sm-12 mt-1">
      <label for="image">Agrega una imagen para la categoria:</label>
      @if ($image && !is_string($image))
        <img src="{{ $image->temporaryUrl() }}" height="150" width="170"
          class="rounded __img-style">
      @endif
      <div class="form-group custom-file">
        <input type="file" class="custom-file-input form-control"
            wire:model.lazy="image"
            accept="image/jpeg,image/png,image/git">
        <label class="custom-file-label">Nombre: {{$image}}</label>
        @error('image')
          <span class="text-danger er">{{$message}}</span>
        @enderror
      </div>
      @if ($selected_id > 0 && is_string($image))
        <div class="text-center mb-3">
          <img src="{{asset('storage/'.$image)}}" alt="imagen ejemplo" height="150" width="170" class="rounded __img-style">
        </div>
      @endif
    </div>

</div>

@include('common.modalFooter')
