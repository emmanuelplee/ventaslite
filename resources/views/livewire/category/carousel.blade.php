<div class="container">
    <!-- <div class="col-sm-10 offset-1"> -->
    <div class="col-sm-12">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{asset('/storage/categories/1_course_test.jpg')}}"
                    class="d-block w-75 mx-auto rounded" height="400px" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('/storage/categories/2_sneaker_test.jpg')}}"
                    class="d-block w-75 mx-auto rounded" height="400px" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('/storage/categories/3_mobile_test.jpg')}}"
                    class="d-block w-75 mx-auto rounded" height="400px" alt="...">
                </div>
            </div>
            <a class="bg-info carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="bg-info carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
            </a>
        </div>
    </div>
</div>




