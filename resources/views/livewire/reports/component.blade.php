<div class="row sales layout-top-spacing">
  <div class="col-sm-12">
    <div class="widget widget-chart-one">
      <div class="widget-heading">
        <h5 class="car-title">
            <b>{{$componentName}}</b>
        </h5>
      </div>
      <div class="widget-content">
        <div class="row">
          <!-- Datos para acciones -->
          <div class="col-sm-12 col-md-3">
            <div class="row">
              <div class="col-sm-12">
                <h6>Elige el ususario</h6>
                <div class="form-group">
                  <select wire:model="userId" class="form-control">
                    <option value="0">Todos</option>
                    @foreach ($users as $user)
                      <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-sm-12">
                <h6>Elige tipo de reporte</h6>
                <div class="form-group">
                  <select class="form-control" wire:model="reportType">
                    <option value="0">Ventas del dia</option>
                    <option value="1">Ventas por fecha</option>
                  </select>
                </div>
              </div>
              <!-- Begin Fechas -->
              <div class="col-sm-12 mt-2">
                <h6>Fecha Desde</h6>
                <div class="form-group flatpickr">
                  <input type="text" wire:model="from_date" class="form-control"
                    placeholder="Click para elegir" data-input>
                </div>
              </div>
              <div class="col-sm-12 mt-2">
                <h6>Fecha Hasta</h6>
                <div class="form-group flatpickr">
                  <input type="text" wire:model="to_date" class="form-control"
                    placeholder="Click para elegir" data-input>
                </div>
              </div>
              <!-- Botones -->
              <div class="col-sm-12">
                  <button wire:click="$refresh"
                      class="btn __agregar btn-block text-white">Consultar
                  </button>

                  <a class="btn __agregar btn-block text-white {{count($data)<1 ? 'disabled': ''}}"
                    href="{{url('report/pdf'.'/'.$userId.'/'.$reportType.'/'.$from_date.'/'.$to_date)}}"
                    target="_blank">Generar PDF</a>

                  <a class="btn __agregar btn-block text-white {{count($data)<1 ? 'disabled': ''}}"
                    href="{{url('report/exel'.'/'.$userId.'/'.$reportType.'/'.$from_date.'/'.$to_date)}}"
                    target="_blank">Exportar a Excel</a>
              </div>
            </div>
          </div>
          <!-- Begin Tabla -->
          <div class="col-sm-12 col-md-9">
            <div class="table-responsive-md">
              <table class="table table-md table-bordered table-striped">
                <thead>
                  <tr class="text-center">
                    <th class="table-th">FOLIO</th>
                    <th class="table-th">TOTAL</th>
                    <th class="table-th">ITEMS</th>
                    <th class="table-th">ESTATUS</th>
                    <th class="table-th">USUARIO</th>
                    <th class="table-th">FECHA</th>
                    <th class="table-th" width="50px">ACCION</th>
                  </tr>
                </thead>
                <tbody>
                  @if (count($data) < 1)
                    <tr class="text-center">
                      <td colspan="7">
                        <h5>Sin Resultados</h5>
                      </td>
                    </tr>
                  @endif
                  @foreach($data as $item)
                  <tr class="text-center">
                    <td>
                      <h6>{{ $item->id }}</h6>
                    </td>
                    <td>
                      <p>${{ number_format($item->total,2)}}</p>
                    </td>
                    <td>
                      <p>{{ $item->items }}</p>
                    </td>
                    <td>
                      <p>{{ $item->status }}</p>
                    </td>
                    <td>
                      <p>{{ $item->user }}</p>
                    </td>
                    <td>
                        <p>
                          {{\Carbon\Carbon::parse($item->created_at)->format('d-m-Y')}}
                        </p>
                    </td>
                    <td>
                      <button  wire:click.prevent="getDetails({{$item->id}})"
                        class="btn __mtmobile bs-tooltip mb-1"
                        data-placement="top" title="Detalles">
                        <i class="fas fa-list"></i>
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
                <!-- <div style="display: flex;justify-content: center;">
                    {{ '$data->links()' }}
                </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('livewire.reports.sale-details')
</div>
<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    flatpickr(document.getElementsByClassName('flatpickr'),{
      enableTime: false,
      wrap: true,
      dateFormat: 'Y-m-d',
      locale: {
        firstDayOfWeek: 1,
        weekdays: {
          shorthand: [
              "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"
          ],
          longhand: [
              "Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado",
          ],
        },

        months: {
          shorthand: [
              "Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic",
          ],
          longhand: [
              "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre",
          ],
        },
      }
    })
    //Eventos
    window.livewire.on('item-modal-show', msg =>{
        console.log(msg);
        $('#modal-details').modal('show')
    })
  })
</script>
