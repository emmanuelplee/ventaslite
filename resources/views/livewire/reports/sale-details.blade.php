<div wire:ignore.self id="modal-details" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-dark">
          <h5 class="modal-title text-white">
            <b>Detalle del venta # {{$saleId}}</b>
          </h5>
          <button class="close" data-dismiss="modal" type="button" aria-label="close">
            <span class="text-white">&times;</span>
          </button>
      </div>

      <div class="modal-body">
        <div class="table-responsive-md">
          <table class="table table-md table-bordered table-striped">
            <thead>
              <tr class="text-center">
                  <th class="table-th">FOLIO</th>
                  <th class="table-th">PRODUCTO</th>
                  <th class="table-th">PRECIO</th>
                  <th class="table-th">CANT</th>
                  <th class="table-th">IMPORTE</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($details as $item)
                <tr class="text-center">
                  <td><h6>{{$item->id}}</h6></td>
                  <td><h6>{{$item->product}}</h6></td>
                  <td><p>${{number_format($item->price,2)}}</p></td>
                  <td><p>${{number_format($item->quantity,0)}}</p></td>
                  <td><p>${{number_format(($item->quantity * $item->price),2)}}</p></td>
                </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr class="text-center">
                <td colspan="3" class="text-right"><h6 class="text-info font-weight-bold">TOTALES:</h6></td>
                <td>
                  @if (count($details) > 0)
                    <h6 class="text-info">{{$countDetails}}</h6>
                  @endif
                </td>

                @if (count($details) > 0)
                  <td>
                    <h6 class="text-info">${{number_format($sumDetails,2)}}</h6>
                  </td>
                @endif
              </tr>
            </tfoot>
          </table>
        </div>
      </div>

    </div>
  </div>
</div>
