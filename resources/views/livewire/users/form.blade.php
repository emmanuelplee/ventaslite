@include('common.modalHead')
<div class="row">
  <!-- Nombre usuario -->
  <div class="col-sm-12 col-md-8 col-lg-6">
    <div class="form-group">
      <label>Nombre usuario: </label>
      <div class="input-group">
        <input type="text"
          wire:model.lazy="name"
          class="form-control __focus_active" autofocus
          placeholder="Ingrese Nombre usuario">
      </div>
      @error('name')
        <span class="text-danger er">{{ $message }}</span>
      @enderror
    </div>
  </div>
  <!-- phone -->
  <div class="col-sm-12 col-md-4 col-lg-6">
    <div class="form-group">
      <label>Numero de telefono:</label>
      <div class="input-group">
        <input type="text"
          wire:model.lazy="phone"
          pattern="[0-9]"
          class="form-control"
          placeholder="Ingresa Numero de telefono"
          maxlength="10">
      </div>
      @error('phone')
        <span class="text-danger er">{{ $message }}</span>
      @enderror
    </div>
  </div>
  <!-- email -->
  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="form-group">
      <label>Email: </label>
      <div class="input-group">
        <input type="email"
          wire:model.lazy="email"
          class="form-control"
          placeholder="Ingresa Email">
      </div>
      @error('email')
        <span class="text-danger er">{{ $message }}</span>
      @enderror
    </div>
  </div>
  <!-- estatus -->
  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="form-group">
      <label>Estatus Usuario:</label>
      <div class="input-group">
        <select wire:model.lazy="status" class="form-control">
          <option value="elegir_status" selected>== Elegir Estatus ==</option>
          <option value="ACTIVE">Activo</option>
          <option value="LOCKED">Bloqueado</option>
        </select>
      </div>
      @error('status')
        <span class="text-danger er">{{ $message }}</span>
      @enderror
    </div>
  </div>
  <!-- roles -->
  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="form-group">
      <label>Role Usuario:</label>
      <div class="input-group">
        <select wire:model.lazy="role" class="form-control">
          <option value="elegir_rol" selected>== Elegir Rol ==</option>
            @foreach ($roles as $rol)
              <option value="{{$rol->id}}">{{$rol->name}}</option>
            @endforeach
        </select>
      </div>
      @error('role')
        <span class="text-danger er">{{ $message }}</span>
      @enderror
    </div>
  </div>
  <!-- password -->
  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="form-group">
      <label>Contraseña: </label>
      <div class="input-group">
        <input type="password"
            wire:model.lazy="password"
            class="form-control">
      </div>
      @error('password')
        <span class="text-danger er">{{ $message }}</span>
      @enderror
    </div>
  </div>
  <!-- IMAGE -->
  <div class="col-sm-12 col-md-12 col-lg-12">
    <label for="image">Agrega una imagen para el usuario: </label>
      @if ($image && !is_string($image))
        <img src="{{ $image->temporaryUrl() }}"
          height="150" width="170" class="rounded __img-style">
      @endif
    <div class="form-group custom-file">
      <input type="file" class="custom-file-input form-control"
        wire:model.lazy="image" accept="image/jpeg,image/png,image/git">
      <label class="custom-file-label">Nombre: {{$image}}</label>
      @error('image')
        <span class="text-danger er">{{$message}}</span>
      @enderror
    </div>
    @if ($selected_id > 0 && is_string($image))
      <div class="text-center mb-3">
        <img src="{{asset('storage/'.$image)}}" alt="imagen ejemplo"
          height="150" width="170" class="rounded __img-style">
      </div>
    @endif
  </div>

</div>
@include('common.modalFooter')
