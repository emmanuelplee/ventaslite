<!-- BEGIN GLOBAL MANDATORY STYLES -->
<script src="{{asset('assets/js/loader.js')}}"></script>
<link href="{{asset('assets/css/loader.css')}}" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
<link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{asset('assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/structure.css')}}" rel="stylesheet" type="text/css" class="structure" />
<link href="{{asset('assets/css/dashboard/dash_2.css')}}" rel="stylesheet" type="text/css" class="dashboard-sales" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
<link href="{{asset('plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('plugins/sweetalerts/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('plugins/notification/snackbar/snackbar.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('plugins/flatpickr/flatpickr.dark.css')}}" rel="stylesheet" type="text/css">

<link href="{{asset('plugins/font-icons/fontawesome/css/fontawesome.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/fontawesome.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

<link href="{{ asset('assets/css/forms/bootstrap-form.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/forms/theme-checkbox-radio.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/widgets/modules-widgets.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/css/apps/scrumboard.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/apps/notes.css') }}" rel="stylesheet" type="text/css" />

<!-- STYLES POR COMPONENTE :V -->
<link href="{{asset('css/style_category.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/style_product.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/style_coins.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/style_pos.css')}}" rel="stylesheet" type="text/css">


<style>
    aside{
        display: none !important;
    }
    .page-item.active .page-link {
        z-index: 3;
        color: #fff;
        background-color: #0A65FF;
        border-color: #0A65FF;
    }
    label{
        color: black !important;
    }
    /* .form-control {
        color:#000 !important;
    } */
    @media (max-width: 480px){
        .__mtmobile{
            margin-bottom: 10px !important;
        }
        .__mbmobile{
            margin-bottom: 10px !important;
            background-color: none !important;
            color: black;
            box-shadow: none
        }
        .hideonsm{
            display: none !important;
        }
        .inblock{
            display: block;
        }
    }
    .__borde {
        border: 2px solid red;
    }
    /* SiderBar background */
    .sidebar-theme #compactSidebar {
        background: #05317d !important;
        width: 110px !important;
    }

    /* Inicio Sidebar */
    .header-container .sidebarCollapse {
        color: #0A65FF !important;
    }
    .base-menu .base-icons {
        color: #ffff !important;
        font-size: 42px !important;
        font-weight: 800 !important;
    }
    .base-menu .base-icons:hover {
        color: #22b8cf !important;
    }
    /* Fin Sidebar */

    /* sweetalerts 2 warning */
    .swal2-icon.swal2-warning {
      font-size: 18px !important;
    }
    .swal2-icon.swal2-info {
      font-size: 18px !important;
    }
    .swal2-popup .swal2-styled.swal2-cancel {
      color: #fff !important;
    }
    /* Boton de Agregar body*/
    .__agregar{
    background-color: #0A65FF !important;
    font-size: 0.95rem !important;
}
    /* Tabla resposnsive body */
    .table-th{
      color: black !important;
    }
    .__img-style{
      object-fit:cover;
      border: 1px solid #ddd;
      border-radius: 4px;
      padding: 5px;
    }

  /* @font-face {
    font-family: 'Material Icons';
    font-style: normal;
    font-weight: 400;
    src: url(plugins/font-icons/iconfont/MaterialIcons-Regular.eot);
    src: local('Material Icons'),
      local('MaterialIcons-Regular'),
      url(plugins/font-icons/iconfont/MaterialIcons-Regular.woff2) format('woff2'),
      url(plugins/font-icons/iconfont/MaterialIcons-Regular.woff) format('woff'),
      url(plugins/font-icons/iconfont/MaterialIcons-Regular.ttf) format('truetype');
  }

  .material-icons {
    font-family: 'Material Icons';
    font-weight: normal;
    font-style: normal;
    font-size: 24px;
    display: inline-block;
    line-height: 1;
    text-transform: none;
    letter-spacing: normal;
    word-wrap: normal;
    white-space: nowrap;
    direction: ltr;
    -webkit-font-smoothing: antialiased;
    text-rendering: optimizeLegibility;
    -moz-osx-font-smoothing: grayscale;
    font-feature-settings: 'liga';
  } */
</style>

@livewireStyles
