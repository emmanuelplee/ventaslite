<div class="sidebar-wrapper sidebar-theme">

  <div id="compact_submenuSidebar" class="submenu-sidebar" style="display: none!important">
  </div>
  <nav id="compactSidebar">
    <ul class="menu-categories">

      <!-- <li class="active">
          <a href="#dashboard" data-active="true" class="menu-toggle">
              <div class="base-menu">
                  <div class="base-icons">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                  </div>
                  <span>PRINCIPAL</span>
              </div>
          </a>
      </li> -->

      @can('Categories_Index')
      <li class="active">
        <a href="{{ url('categories') }}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
                <i class="fas fa-th-large"></i>
            </div>
            <span>CATEGORIAS</span>
          </div>
        </a>
      </li>
      @endcan

      @can('Products_Index')
      <li>
        <a href="{{ url('products') }}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
              <i class="fas fa-clipboard"></i>
            </div>
            <span>PRODUCTOS</span>
          </div>
        </a>
      </li>
      @endcan

      @can('Sales_Index')
      <li>
        <a href="{{ url('pos') }}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
              <i class="fas fa-shopping-cart"></i>
            </div>
            <span>VENTAS</span>
          </div>
        </a>
      </li>
      @endcan

      @can('Roles_Index')
      <li>
        <a href="{{url('roles')}}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
              <i class="fas fa-key"></i>
            </div>
            <span>ROLES</span>
          </div>
        </a>
      </li>
      @endcan

      @can('Permissions_Index')
      <li>
        <a href="{{url('permisos')}}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
              <i class="fas fa-check-square"></i>
            </div>
            <span>PERMISOS</span>
          </div>
        </a>
      </li>
      @endcan

      @can('Asignar_Index')
      <li class="asignar">
        <a href="{{url('asignar')}}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
              <i class="fas fa-eye"></i>
            </div>
            <span>ASIGNAR</span>
          </div>
        </a>
      </li>
      @endcan

      @can('Users_Index')
      <li>
        <a href="{{url('users')}}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
              <i class="fas fa-users"></i>
            </div>
            <span>USUARIOS</span>
          </div>
        </a>
      </li>
      @endcan

      @can('Coins_Index')
      <li>
        <a href="{{ url('coins') }}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
              <i class="fas fa-coins"></i>
            </div>
            <span>MONEDAS</span>
          </div>
        </a>
      </li>
      @endcan

      <li>
        <a href="{{url('cashout')}}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
              <i class="fas fa-dollar-sign"></i>
            </div>
            <span>CORTE CAJA</span>
          </div>
        </a>
      </li>

      <li>
        <a href="{{url('reports')}}" data-active="false" class="menu-toggle">
          <div class="base-menu">
            <div class="base-icons">
              <i class="fas fa-chart-bar"></i>
            </div>
            <span>REPORTES</span>
          </div>
        </a>
      </li>

    </ul>
  </nav>

</div>

