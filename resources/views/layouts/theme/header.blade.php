<div class="header-container fixed-top">
  <header class="header navbar navbar-expand-sm ">
    <ul class="navbar-item flex-row __header">
      <li class="theme-logo" style=" margin-right: 20px;">
        <a href="#" class=" text-center __logo">
         <!-- <img src="{{asset('assets/img/logo_v1.jpg')}}"
              class="navbar-logo" alt="logo"> -->
              <div>R-al</div>
              <div>System</div>
        </a>
      </li>
    </ul>

    <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
      <span class="__icon_search">
        <i class="fas fa-list-ul fa-lg"></i>
    </span>
    </a>

      <!-- Buscador del header en componente-->
    <livewire:search-controller>

    <ul class="navbar-item flex-row navbar-dropdown">

      <li class="nav-item dropdown user-profile-dropdown  order-lg-0 order-1">
        <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="icon-container">
            <span style="color:#22b8cf">
              <i class="fas fa-user-circle fa-lg"></i>
            </span>
          </div>
            <!-- <img src="{{ asset('assets/img/profile.png')}}" alt="admin-profile" class="img-fluid"> -->
        </a>
        <div class="dropdown-menu position-absolute animated fadeInUp" aria-labelledby="userProfileDropdown">
          <div class="user-profile-section">
            <div class="media mx-auto">
              <img src="{{ asset('assets/img/profile.png')}}" class="img-fluid mr-2" alt="img">
              <div class="media-body">
                  @if (Auth::user())
                    <h5>{{Auth::user()->name}}</h5>
                    @php
                      $nameRole=Spatie\Permission\Models\Role::find(Auth::user()->role_id);
                    @endphp
                    <p>Perfil: {{$nameRole->name}}</p>
                  @endif
              </div>
            </div>
          </div>
          <div class="dropdown-item">
            <a href="user_profile.html">
              <i class="fas fa-user"></i>
              <span>Mi Perfil</span>
            </a>
          </div>
          <div class="dropdown-item">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
              <i class="fas fa-sign-out-alt"></i>
              <span>Salir</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </div>
        </div>
      </li>
    </ul>
  </header>
</div>
<style>
    .header{
      padding-left:0;
      background: #0F2027 !important;
      background: -webkit-linear-gradient(to top, #2C5364, #203A43, #0F2027) !important;
      background: linear-gradient(to top, #2C5364, #203A43, #0F2027) !important;

    }
    .__header{
        padding-left: 15px !important;
        background-color:#05317d;
        width: 110px;
        height: 80px;
    }
    .__logo{
        /* margin-left:20px; */
        color: white;
        font-size: 20px;
        font-weight: 700;
    }
    .__logo:hover{
        color: #22b8cf;
        width: 38px !important;
        height: 38px !important;
    }
    .navbar .navbar-item .nav-item form.form-inline input.search-form-control {
      background-color: #fff !important;
      padding-top: 12px;
      border: 2px solid #0A65FF !important;
      color: #000 !important;
      border-radius: 20px !important;
    }
    .__icon_search{
        margin-left:15px !important;
        color: #22b8cf;
    }
</style>
