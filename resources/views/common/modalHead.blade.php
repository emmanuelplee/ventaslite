<div wire:ignore.self class="modal fade" id="theModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0A65FF">
        <h6 class="modal-title text-white">
            <b>{{$componentName}}</b> | {{$selected_id > 0 ? 'EDITAR' : 'CREAR'}}
        </h6>
        <h6 class="text-center text-warning" wire:loading>POR FAVOR ESPERE...</h6>
      </div>
      <div class="modal-body">
        <!-- <p>Modal body text goes here.</p> -->
