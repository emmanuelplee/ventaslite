<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Denomination extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'value',
        'image'
    ];
    /* Accesor para imagen */
    public function getImageAccessorAttribute(){
        if ($this->image != null) {
            $path = public_path('/storage/coins/' . $this->image);
            return file_exists($path) ? 'coins/' . $this->image : 'noimg.jpg';
        }else{
            return 'noimg.jpg';
        }
    }
}
