<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable = [
        'total',
        'items',
        'cash',
        'change',
        'status',
        'user_id'
    ];

    // public function product(){
    //     return $this->belongsTo(Product::class);
    // }
    public function products(){
        return $this->hasMany(Sale::class);
    }
}
