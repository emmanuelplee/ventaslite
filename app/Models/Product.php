<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'barcode',
        'cost',
        'price',
        'stock',
        'alerts',
        'image',
        'category_id'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    // public function sales(){
    //     return $this->belongsTo(Product::class);
    // }
    /* Accesor para imagen */
    public function getImageAccessorAttribute(){
        if ($this->image != null) {
            $path = public_path('/storage/products/' . $this->image);
            return file_exists($path) ? 'products/' . $this->image : 'noimg.jpg';
        }else{
            return 'noimg.jpg';
        }
    }
}
