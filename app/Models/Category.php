<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'description',
        'image'
    ];

    public function products(){
        return $this->hasMany(Product::class);
    }
    /* Imagen default para la lista de categories */
    public function getImageAccessorAttribute(){
        if ($this->image == null) {
            return 'noimg.jpg';
        }
        $path = public_path('/storage/categories/'. $this->image);
        if (file_exists($path)) {
            return 'categories/' . $this->image;
        }else{
            return 'noimg.jpg';
        }
    }
}
