<?php

namespace App\Http\Livewire;

use App\Models\Sale;
use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Spatie\Permission\Models\Role;

class UsersController extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $pageTitle, $componentName, $search;
    public $selected_id, $name, $phone, $email, $role,
           $status, $password, $image, $fileLoaded;

    private $pagination = 3;

    public function mount(){
        $this->componentName    = 'Usuarios';
        $this->pageTitle        = 'Listado';
        $this->selected_id      = 0;
        $this->status           = 'elegir_status';
        $this->role             = 'elegir_rol';
    }

    public function paginationView(){
        return 'vendor.livewire.bootstrap';
    }

    public function render()
    {
        if (strlen($this->search) > 0) {
            $data = User::join('roles as r','r.id','users.role_id')
                ->select('users.*','r.name as role')
                ->where('users.name','like','%'.$this->search.'%')
                ->orderBy('users.name','asc')
                ->paginate($this->pagination);
        }else{
            $data = User::join('roles as r','r.id','users.role_id')
                ->select('users.*','r.name as role')
                ->orderBy('users.name','asc')
                ->paginate($this->pagination);
        }
        $roles = Role::orderBy('name','asc')->get();
        return view('livewire.users.component', [
            'data' => $data,
            'roles' => $roles
            ])
        ->extends('layouts.theme.app')
        ->section('content');
    }
    public function store()
    {
      $rules = [
          'name'        => 'required|min:3',
          'email'       => 'required|unique:users|email',
          'password'    => 'required|min:3',
          'status'      => 'required|not_in:elegir_status',
          'role'        => 'required|not_in:elegir_rol',
      ];
      $messages = [
          'name.required'       => 'El nombre de usuario es requerido',
          'name.min'            => 'El nombre de usuario debe tener al menos 3 caracteres',
          'email.required'      => 'El email es requerido',
          'email.email'         => 'El email no es valido',
          'email.unique'        => 'El email ya existe en el sistema',
          'password.required'   => 'Ingresa la contraseña',
          'password.min'        => 'La contraseña debe tener al menos 3 caracteres',
          'status.required'     => 'Secciona el estatus para el usuario',
          'status.not_in'       => 'El campo debe ser diferente a Elegir Estatus',
          'role.required'    => 'Secciona el perfil/rol del usuario',
          'role.not_in'      => 'El campo debe ser diferente a Elegir Rol',
      ];
      $this->validate($rules, $messages);

      $data_user = User::create([
          'name' => $this->name,
          'email' => $this->email,
          'phone' => $this->phone,
          'status' => $this->status,
          'role_id' => $this->role,
          'password' => bcrypt($this->password)
        ]);
        $data_user->syncRoles($this->role);
        if ($this->image) {
            $customFileName = uniqid() . '_.' . $this->image->extension();
            $this->image->storeAs('public/users', $customFileName);
            $data_user->image = $customFileName;
            $data_user->save();
        }

      $this->resetUI();
      $this->emit('item-added','Registro exitoso!');
    }
    public function edit($id)
    {
      $user = User::find($id);
      if (isset($user)) {
          $this->selected_id  = $user->id;
          $this->name         = $user->name;
          $this->phone        = $user->phone;
          $this->email        = $user->email;
          $this->role         = $user->role_id;
          $this->status       = $user->status;
          $this->password     = '';

        /* BEGIN imagen default en el Form categories */
        $path = public_path('/storage/users/' . $user->image);
        if (file_exists($path)) {
            $this->image = 'users/' . $user->image;
        }else {
            $this->image = 'noimg.jpg';
        }
        if ($user->image == null) {
            $this->image = 'noimg.jpg';
        }
        /* END imagen en el Form categories */

          $this->emit('item-modal-show', 'Mostar modal del Registro!');
          return;
      }else {
          $this->emit('item-error', 'No existe el registro!');
          return;
      }
    }
    public function update()
    {
        $rules = [
            'name'        => 'required|min:3',
            'email'       => "required|email|unique:users,email,{$this->selected_id}",
            'status'      => 'required|not_in:elegir_status',
            'role'        => 'required|not_in:elegir_rol',
            'password'    => 'required|min:3',
        ];
        $messages = [
            'name.required'       => 'El nombre del usuario es requerido',
            'name.min'            => 'El nombre del usuario debe tener al menos 3 caracteres',
            'email.required'      => 'El email es requerido',
            'email.email'         => 'El el email',
            'name.unique'         => 'El email ya existe en el sistema',
            'name.unique'         => 'El email ya existe en el sistema',
            'status.required'     => 'Secciona el estatus para el usuario',
            'status.not_in'       => 'El campo debe ser diferente a Elegir Estatus',
            'role.required'       => 'Secciona el perfil/rol del usuario',
            'role.not_in'         => 'El campo debe ser diferente a Elegir Rol',
            'password.required'   => 'Ingresa la contraseña',
            'password.min'        => 'La contraseña debe tener al menos 3 caracteres',
        ];
        $this->validate($rules, $messages);

        $user = User::find($this->selected_id);
        // dd($this->role);
        $user->update([
            'name'      => $this->name,
            'email'     => $this->email,
            'phone'     => $this->phone,
            'status'    => $this->status,
            'role_id'   => $this->role,
            'password'  => bcrypt($this->password)
        ]);

        $user->syncRoles($this->role);

        if ($this->image && !is_string($this->image)) {
            $customFileName = uniqid() . '_.' . $this->image->extension();
            $this->image->storeAs('public/users/', $customFileName);
            $imageTemp = $user->image; //Imagen temporal

            $user->image = $customFileName;
            $user->save();

            if ($imageTemp != null) {
                $path = public_path('/storage/users/' . $imageTemp);
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        }
        $this->resetUI();
        $this->emit('item-updated','Usuario Actualizado!');
    }
    protected $listeners = ['deleteRow' => 'destroy'];

    public function destroy($id){
        $data_user = User::find($id);
        // dd($data_user);
        if (isset($data_user)) {
            $sale = Sale::where('user_id',$data_user->id)->count();
            if ($sale > 0) {
                $this->emit('user-withsales','No es posible elimianar el usuario por que tiene ventas registradas');
            }else {
                $imageTemp = $data_user->image;
                $data_user->delete();

                if ($imageTemp != null) {
                    $path = public_path('/storage/users' . $imageTemp);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }
                $this->resetUI();
                $this->emit('item-deleted','Usuario Eliminado!');
            }
        }else
        $this->emit('product-deleted','Producto no se pudo Eliminar!');
    }

    public function resetUI()
    {
        $this->name     = '';
        $this->phone    = '';
        $this->email    = '';
        $this->status   = 'elegir_status';
        $this->role     = 'elegir_rol';
        $this->password = '';
        $this->image    = '';
        $this->selected_id  = 0;
        $this->resetValidation();
        $this->resetPage();
    }
}
