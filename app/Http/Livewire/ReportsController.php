<?php

namespace App\Http\Livewire;

use App\Models\Sale;
use App\Models\SaleDetail;
use App\Models\User;
use Carbon\Carbon;
use Livewire\Component;

class ReportsController extends Component
{
    public $componentName,$data,$userId,$reportType,$from_date,$to_date;

    public $saleId,$details,$countDetails,$sumDetails;

    public function mount(){
        $this->componentName = 'Reportes de Ventas';
        $this->data = [];
        $this->userId       = 0;
        $this->reportType   = 0;
        $this->from_date   = null;
        $this->to_date   = null;
        $this->saleId   = 0;
        $this->details   = [];
        $this->countDetails   = 0;
        $this->sumDetails   = 0;
    }
    public function render()
    {

        $data = $this->saleByDate();

        return view('livewire.reports.component',[
            'users' => User::orderBy('name','asc')->get(),
            'data'  => $data
        ])
            ->extends('layouts.theme.app')
            ->section('content');
    }

    public function saleByDate()
    {
        if ($this->reportType == 0) {
            $from = Carbon::parse(Carbon::now())->format('Y-m-d') . ' 00:00:00';
            $to = Carbon::parse(Carbon::now())->format('Y-m-d') . ' 23:59:59';
            $this->from_date = '';
            $this->to_date = '';
        }else {
            $from = Carbon::parse($this->from_date)->format('Y-m-d') . ' 00:00:00';
            $to = Carbon::parse($this->to_date)->format('Y-m-d') . ' 23:59:59';
        }

        if ($this->reportType >= 1 && ($this->from_date=='' || $this->to_date=='')) {
            $this->data = [];
            return;
        }

        if ($this->userId == 0) {
            $this->data = Sale::join('users as u','u.id','sales.user_id')
            ->select('sales.*','u.name as user')
            ->whereBetween('sales.created_at',[$from,$to])
            ->get();
        }else {
            $this->data = Sale::join('users as u','u.id','sales.user_id')
            ->select('sales.*','u.name as user')
            ->whereBetween('sales.created_at',[$from,$to])
            ->where('sales.user_id', $this->userId)
            ->get();
        }
    }
    public function getDetails($saleId)
    {
        $this->details = SaleDetail::join('products as p','p.id','sale_details.product_id')
        ->select('sale_details.id','sale_details.price','sale_details.quantity','p.name as product')
        ->where('sale_details.sale_id',$saleId)
        ->get();

        $suma = $this->details->sum(function($item){
            return ($item->price * $item->quantity);
        });
        $this->sumDetails = $suma;
        $this->countDetails = $this->details->sum('quantity');
        $this->saleId = $saleId;

        $this->emit('item-modal-show','Detalles cargados');
    }
}
