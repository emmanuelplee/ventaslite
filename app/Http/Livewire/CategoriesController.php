<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use Livewire\WithFileUploads; //trait
use Livewire\WithPagination; //trait

class CategoriesController extends Component
{

	use WithFileUploads;
	use WithPagination;

	public $selected_id, $name, $description, $image,
		$pageTitle, $componentName, $search;
	private $pagination = 5;

	public function mount()
	{
		$this->pageTitle = 'Listado';
		$this->componentName = 'Categorias';
	}

	public function paginationView()
	{
		return 'vendor.livewire.bootstrap';
	}

	public function render()
	{
		if (strlen($this->search) > 0) {
			$data = Category::where('name', 'like', '%' . $this->search . '%')
				->paginate($this->pagination);
		} else {
			$data = Category::orderBy('id', 'desc')
				->paginate($this->pagination);
		}
		// echo '<pre>';
		// print_r($data);
		// die();
		// $data = Category::all();
		return view('livewire.category.categories', ['data' => $data])
			->extends('layouts.theme.app')
			->section('content');
	}

	public function edit($id)
	{
		// error_log('Edit categories');
		// error_log($id);
		$record = Category::find($id, ['id', 'name', 'description', 'image']);
		// echo '<pre>'; print_r($record);
		if (isset($record)) {
            $this->selected_id  = $record->id;
			$this->name         = $record->name;
			$this->description  = $record->description;

            /* BEGIN imagen default en el Form categories */
            // $image = new \App\Models\Category();
            // $this->image        = $image->getImageAccessorAttribute();
            $path = public_path('/storage/categories/' . $record->image);
            if (file_exists($path)) {
                $this->image = 'categories/' . $record->image;
            }
            else {
                $this->image = 'noimg.jpg';
            }
            if ($record->image == null) {
                $this->image = 'noimg.jpg';
            }
            /* END imagen en el Form categories */

			$this->emit('modal-show-item', 'show modal!');
		}
	}

	public function store()
	{
		$rules = [
			'name'          => 'required|unique:categories|min:3',
			'description'   => 'required',
            'image'         => 'max:1024'
		];
		$messages = [
			'name.required'         => 'El nombre de la categoria es requerido',
			'name.unique'           => 'La categoria ya existe',
			'name.min'              => 'El nombre debe tener al menos 3 caracteres',
			'description.required'  => 'La descripcion es requerida',
            'image.max'             => 'El tamaño maximo es 1 mega'
		];

		$this->validate($rules, $messages);

		$data = Category::create([
			'name'          => strtoupper($this->name),
			'description'   => $this->description,
		]);
		// dd($this->image);
		if ($this->image) {
			$customFileName = uniqid() . '_.' . $this->image->extension();
			$this->image->storeAs('public/categories/', $customFileName);
			$data->image = $customFileName;
			$data->save();
		}
		// $this->resetErrorBag();
		$this->resetUI();
		$this->emit('item-added', 'Categoria Registrada');
	}

	public function update()
	{
		$rules = [
			'name'          => "required|min:3|unique:categories,name,{$this->selected_id}",
			'description'   => 'required',
            'image'         => 'max:1024'
		];

		$messages = [
			'name.required'         => 'El nombre es requerido',
			'name.min'              => 'El nombre debe tener al menos 3 caracteres',
			'name.unique'           => 'El nombre de la categoria ya existe',
			'description.required'  => 'La descripcion es requerida',
            'image.max'             => 'El tamaño maximo es 1 mega'
		];

		$this->validate($rules, $messages);

		$data = Category::find($this->selected_id);
		$data->update([
			'name'          => strtoupper($this->name),
			'description'   => $this->description,
		]);
		// echo '<pre>';
		// print_r($this->image);
		if ($this->image && !is_string($this->image)) {
			$customFileName = uniqid() . '_.' . $this->image->extension();
			$this->image->storeAs('public/categories/', $customFileName);
			$imageName = $data->image;

			$data->image = $customFileName;
			// error_log($data->image);
			$data->save();

			if ($imageName != null) {
				$path = public_path('/storage/categories/' . $imageName);
				// error_log($path);
				if (file_exists($path)) {
					// error_log('unlink');
					unlink($path);
				}
			}
		}

		// $this->resetValidation();
		// $this->reset(['name','search','selected_id','iamge']); //MISMO resetUI()
		$this->resetUI();
		$this->emit('item-update', 'Categoria Actualizada!');
	}

	/* Escuchar evento para eliminar desde font-end con js */

	protected $listeners = [
		'deleteRow' => 'destroy',
	];

	public function destroy($id)
	{
		$data = Category::find($id);
		// dd($data);
		if (isset($data)) {
			$imageName = $data->image;
			$data->delete();

			if ($imageName != null) {
				$path = public_path('/storage/categories/' . $imageName);
				// error_log($path);
				if (file_exists($path)) {
					// error_log('unlink');
					unlink($path);
				}
			}
			$this->resetUI();
			$this->emit('item-deleted', 'Categoria Eliminada!');
		} else {
			$this->emit('item-deleted', 'Error no se encuentra el registro');
		}
	}

	public function resetUI(){
		$this->resetValidation();
        $this->resetPage();

		$this->selected_id  = 0;
		$this->name         = '';
		$this->description  = '';
		$this->search       = '';
		$this->image        = null;
	}
}
