<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Denomination;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class CoinsController extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $selected_id, $componentName, $pageTitle, $search,
        $type, $value, $image;

    private $pagination = 5;

    public function mount(){
        $this->componentName    = 'Monedas';
        $this->pageTitle        = 'Listado';
        $this->selected_id      = 0;
        $this->type             = 'ELEGIR';
    }

	public function paginationView()
	{
		return 'vendor.livewire.bootstrap';
	}

	public function render()
	{
		if (strlen($this->search) > 0) {
			$data = Denomination::where('type', 'like', '%' . $this->search . '%')
				->paginate($this->pagination);
		} else {
			$data = Denomination::where('value', '<>', 0)->orderBy('id', 'desc')
				->paginate($this->pagination);
		}
		return view('livewire.denominations.component', ['data' => $data])
			->extends('layouts.theme.app')
			->section('content');
	}

	public function edit($id)
	{
		$record = Denomination::find($id, ['id', 'type', 'value', 'image']);
		// echo '<pre>'; print_r($record);
		if (isset($record)) {
            $this->selected_id  = $record->id;
			$this->type         = $record->type;
			$this->value        = $record->value;

            /* BEGIN imagen default en el Form coins */
            $path = public_path('/storage/coins/' . $record->image);
            if (file_exists($path)) {
                $this->image = 'coins/'.$record->image;
            }
            else {
                $this->image = 'noimg.jpg';
            }
            if ($record->image == null) {
                $this->image = 'noimg.jpg';
            }
            /* END imagen en el Form categories */

			$this->emit('item-modal-show', 'Mostar modal del Registro!');
		}
	}

	public function store()
	{
		$rules = [
			'type'    => 'required|not_in:ELEGIR',
			'value'   => 'required|unique:denominations'
		];
		$messages = [
			'type.required'     => 'El tipo de la moneda es requerido',
			'type.not_in'       => 'El campo debe ser diferente a ELEGIR',
			'value.required'    => 'El valor es requerida',
			'value.unique'      => 'Ya existe el valor'
		];

		$this->validate($rules, $messages);

		$data = Denomination::create([
			'type'  => $this->type,
			'value' => $this->value,
		]);
		if ($this->image) {
			$customFileName = uniqid() . '_.' . $this->image->extension();
			$this->image->storeAs('public/coins/', $customFileName);
			$data->image = $customFileName;
			$data->save();
		}
		$this->resetUI();
		$this->emit('item-added', 'Registro exitoso!');
	}

	public function update()
	{
		$rules = [
			'type'    => 'required|not_in:ELEGIR',
			'value'   => "required|unique:denominations,value,{$this->selected_id}"
		];

		$messages = [
			'type.required'     => 'El tipo de la moneda es requerido',
			'type.not_in'       => 'El campo debe ser diferente a ELEGIR',
			'value.required'    => 'El valor es requerida',
			'value.unique'      => 'Ya existe el valor'
		];

		$this->validate($rules, $messages);

		$data = Denomination::find($this->selected_id);
		$data->update([
			'type'  => $this->type,
			'value' => $this->value,
		]);
		// echo '<pre>'; print_r($this->image);
		if ($this->image && !is_string($this->image)) {
			$customFileName = uniqid() . '_.' . $this->image->extension();
			$this->image->storeAs('public/coins/', $customFileName);
			$imageTemp = $data->image;

			$data->image = $customFileName;
			$data->save();

			if ($imageTemp != null) {
				$path = public_path('/storage/coins/' . $imageTemp);
				if (file_exists($path)) {
					unlink($path);
				}
			}
		}
		$this->resetUI();
		$this->emit('item-updated', 'Registro Actualizado!');
	}

	/* Escuchar evento para eliminar desde font-end con js */
	protected $listeners = ['deleteRow' => 'destroy',];

	public function destroy($id)
	{
		$data = Denomination::find($id);
		// dd($data);
		if (isset($data)) {
			$imageTemp = $data->image;
			$data->delete();

			if ($imageTemp != null) {
				$path = public_path('/storage/coins/' . $imageTemp);
				if (file_exists($path)) {
					unlink($path);
				}
			}
			$this->resetUI();
			$this->emit('item-deleted', 'Registro Eliminado!');
		} else {
			$this->emit('item-deleted', 'Registro no encontrado!');
		}
	}

	public function resetUI(){
		$this->resetValidation();
        $this->resetPage();

		$this->selected_id  = 0;
		$this->type         = '';
		$this->value        = '';
		$this->search       = '';
		$this->image        = null;
	}
}
