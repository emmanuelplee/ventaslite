<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;
use App\Models\Product;
use Livewire\WithFileUploads; //trait
use Livewire\WithPagination; //trait

class ProductsController extends Component
{

    use WithFileUploads;
    use WithPagination;

    public $selected_id, $category_id, $name, $description,
        $barcode,$cost,$price, $stock, $alerts,$image,
        $pageTitle, $componentName, $search;

    private $pagination = 5;

    public function mount(){
        $this->pageTitle        = 'Listado';
        $this->componentName    = 'Productos';
        $this->category_id      = 'ELEGIR';
    }

    public function paginationView(){
        return 'vendor.livewire.bootstrap';
    }

    public function render()
    {
        if (strlen($this->search) > 0) {
            $data_products = Product::join('categories as c','c.id','products.category_id')
                    ->select('products.*','c.name as category')
                    ->where('products.name','like','%' . $this->search . '%')
                    ->orWhere('products.barcode','like','%' . $this->search . '%')
                    ->orWhere('c.name','like','%' . $this->search . '%')
                    ->orderBy('products.id', 'desc')
                    ->paginate($this->pagination);
        }else {
            $data_products = Product::join('categories as c','c.id','products.category_id')
                    ->select('products.*','c.name as category')
                    ->orderBy('products.id', 'desc')
                    ->paginate($this->pagination);
        }
        return view('livewire.products.component', [
            'data'          => $data_products,
            'categories'    => Category::orderBy('name','asc')->get(),
            ])->extends('layouts.theme.app')
            ->section('content');
    }

    public function edit(Product $product){
        $this->selected_id  = $product->id;
        $this->name         = $product->name;
        $this->description  = $product->description;
        $this->barcode      = $product->barcode;
        $this->cost         = $product->cost;
        $this->price        = $product->price;
        $this->stock        = $product->stock;
        $this->alerts       = $product->alerts;
        $this->category_id  = $product->category_id;

        /* BEGIN imagen default en el Form categories */
        $path = public_path('/storage/products/' . $product->image);
        if (file_exists($path)) {
            $this->image = 'products/' . $product->image;
        }else {
            $this->image = 'noimg.jpg';
        }
        if ($product->image == null) {
            $this->image = 'noimg.jpg';
        }
        /* END imagen en el Form categories */

        $this->emit('modal-show-item','Show modal Edit!');
    }

    public function store(){
        $rules = [
            'name'          => 'required|min:3|unique:products',
            'description'   => 'required',
            'cost'          => 'required',
            'price'         => 'required',
            'stock'         => 'required',
            'alerts'        => 'required',
            'category_id'   => 'required|not_in:ELEGIR',
        ];
        $messages = [
            'name.required'         => 'Nombre del productos requerido',
            'name.min'              => 'El nombre debe tener al menos 3 caracteres',
            'name.unique'           => 'Ya existe el nombre del producto',
            'description.required'  => 'La descripcion es requerida',
            'cost.required'         => 'El costo es requerido',
            'price.required'        => 'El pricio es requerido',
            'stock.required'        => 'El stock es requerido',
            'alerts.required'       => 'Ingresa el valor minimo de existencias',
            'category_id.not_in'    => 'Elige un nombre de categoria diferente a ELEGIR'
        ];

        $this->validate($rules, $messages);

        $data_products = Product::create([
            'name'          => strtoupper($this->name),
            'description'   => $this->description,
            'barcode'       => $this->barcode,
            'cost'          => $this->cost,
            'price'         => $this->price,
            'stock'         => $this->stock,
            'alerts'        => $this->alerts,
            'category_id'   => $this->category_id,
        ]);

        if ($this->image) {
            $customFileName = uniqid() . '_.' . $this->image->extension();
            $this->image->storeAs('public/products', $customFileName);
            $data_products->image = $customFileName;
            $data_products->save();
        }

        $this->resetUI();
        $this->emit('item-added', 'Producto Agregado!');

    }

    public function update(){
        $rules = [
            'name'          => "required|min:3|unique:products,name,{$this->selected_id}",
            'description'   => 'required',
            'cost'          => 'required',
            'price'         => 'required',
            'stock'         => 'required',
            'alerts'        => 'required',
            'category_id'   => 'required|not_in:ELEGIR',
        ];
        $messages = [
            'name.required'         => 'Nombre del productos requerido',
            'name.min'              => 'El nombre debe tener al menos 3 caracteres',
            'name.unique'           => 'Ya existe el nombre del producto',
            'description.required'  => 'La descripcion es requerida',
            'cost.required'         => 'El costo es requerido',
            'price.required'        => 'El pricio es requerido',
            'stock.required'        => 'El stock es requerido',
            'alerts.required'       => 'Ingresa el valor minimo de existencias',
            'category_id.not_in'    => 'Elige un nombre de categoria diferente a ELEGIR'
        ];

        $this->validate($rules, $messages);

        $data_products = Product::find($this->selected_id);

        $data_products->update([
            'name'          => strtoupper($this->name),
            'description'   => $this->description,
            'barcode'       => $this->barcode,
            'cost'          => $this->cost,
            'price'         => $this->price,
            'stock'         => $this->stock,
            'alerts'        => $this->alerts,
            'category_id'   => $this->category_id,
        ]);
        if ($this->image && !is_string($this->image)) {
            $customFileName = uniqid() . '_.' . $this->image->extension();
            $this->image->storeAs('public/products/', $customFileName);
            $imageTemp = $data_products->image; //Imagen temporal

            $data_products->image = $customFileName;
            $data_products->save();

            if ($imageTemp != null) {
                $path = public_path('/storage/products/' . $imageTemp);
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        }
        $this->resetUI();
        $this->emit('item-updated','Producto Actualizado!');
    }

    protected $listeners = ['deleteRow' => 'destroy'];

    public function destroy($id){
        $data_products = Product::find($id);
        // dd($data_products);
        if (isset($data_products)) {
            $imageTemp = $data_products->image;
            $data_products->delete();

            if ($imageTemp != null) {
                $path = public_path('/storage/products' . $imageTemp);
                if (file_exists($path)) {
                    unlink($path);
                }
            }
            $this->resetUI();
            $this->emit('item-deleted','item Eliminado!');
        }else
        $this->emit('product-deleted','Producto no se pudo Eliminar!');
    }

    public function resetUI(){
        $this->resetValidation();
        $this->resetPage();

        $this->selected_id  = 0;
        $this->name         = '';
        $this->description  = '';
        $this->barcode      = '';
        $this->cost         = '';
        $this->price        = '';
        $this->stock        = '';
        $this->alerts       = '';
        $this->category_id  = 'ELEGIR';
        $this->image        = null;
    }
}
