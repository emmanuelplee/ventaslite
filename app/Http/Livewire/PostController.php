<?php

namespace App\Http\Livewire;

use App\Models\Denomination;
use App\Models\Product;
use App\Models\Sale;
use App\Models\SaleDetail;
use Darryldecode\Cart\Facades\CartFacade as Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Livewire\Component;

class PostController extends Component
{
    public $total;/* Total a pagar */
    public $itemsQuantity;/* Articulos en el carrito */
    public $efectivo;/* Dinero con el que paga el cliente*/
    public $change;/* cambio de la compra */

    public function mount()
    {
        $this->efectivo         = 0;
        $this->change           = 0;
        $this->total            = Cart::getTotal();
        $this->itemsQuantity    = Cart::getTotalQuantity();
    }
    public function render()
    {
        $coins = Denomination::OrderBy('value', 'desc')->get();
        return view('livewire.pos.component', [
                'coins' => $coins->sortByDesc('value'),
                'cart'  => Cart::getContent()->sortBy('name')
            ])
        ->extends('layouts.theme.app')
        ->section('content');
    }

    public function aCash( $value )
    {
        $this->efectivo += ($value == 0 ? $this->total : $value);
        $this->change = ($this->efectivo - $this->total);
    }

    protected $listeners = [
        'scan-code'      => 'scanCode',
        'removeItem'    => 'removeItem',
        'clearCart'     => 'clearCart',
        'clearCash'     => 'clearCash',
        'saveSale'      => 'saveSale',
    ];

    public function scanCode($barcode , $cant = 1)
    {
        $product = Product::where('barcode', $barcode)->first();

        if ($product == null || empty($product)) {
            // dd($product);
            $this->emit('scan-notfound', 'El producto no esta Registrado!');
        }else {
            if ($this->InCart($product->id)) {
                $this->increaseQty($product->id);
                return;
            }

            if ($product->stock < 1) {
                $this->emit('no-stock', 'Stock insuficiente :/');
                return;
            }
            /* Agregamos al carrito con imagen */
            Cart::add($product->id, $product->name, $product->price , $cant, $product->image);
            // dd(Cart::getContent());
            $this->total = Cart::getTotal();
            $this->itemsQuantity = Cart::getTotalQuantity();

            $this->emit('scan-ok', 'Producto agregado');
        }
    }

    public function InCart($productId)
    {
        $exits = Cart::get($productId);
        if ($exits) {
            return true;
        }else{
            return false;
        }
    }
    /* detail incrementa la cantidad de articulo */
    public function increaseQty($productId, $cant = 1)
    {
        $title = '';
        $product = Product::find($productId);
        $exits = Cart::get($productId);
        if ($exits) {
            $title = 'Cantidad Actualizado';
        }else {
            $title = 'Producto Agregado';
        }
        if ($exits) {
            if ($product->stock < ($cant + $exits->quantity)) {
                $this->emit('no-stock-plus', 'Stock insufuciente :/');
                return;
            }
        }

        Cart::add($product->id, $product->name, $product->price , $cant, $product->image);

        $this->total = Cart::getTotal();
        $this->itemsQuantity = Cart::getTotalQuantity();
        $this->clearCash();

        $this->emit('scan-ok', $title);
    }
    /* detail update cantidad de articulo */
    public function updateQty($productId, $cant = 1)
    {
        $title = '';
        $product = Product::find($productId);
        $exits = Cart::get($productId);
        if ($exits) {
            $title = 'Cantidad Actualizado';
        }else {
            $title = 'Producto Agregado';
        }
        if ($exits) {
            // dd($exits);
            if ($product->stock < $cant) {
                $this->emit('no-stock', 'Stock insuficiente :/');
                return;
            }
        }
        $this->removeItem($productId);

        if ($cant > 0) {
            Cart::add($product->id, $product->name, $product->price , $cant, $product->image);
            $this->total = Cart::getTotal();
            $this->itemsQuantity = Cart::getTotalQuantity();
            $this->clearCash();

            $this->emit('scan-ok', $title);

        }
    }

    public function removeItem($productId)
    {
        Cart::remove($productId);

        $this->total = Cart::getTotal();
        $this->itemsQuantity = Cart::getTotalQuantity();

        $this->emit('scan-ok', 'Producto Eliminado');
    }
    /* detail decrementa la cantidad de articulos */
    public function decreaseQty($productId)
    {
        $item = Cart::get($productId);
        Cart::remove($productId);

        $newQty = ($item->quantity) - 1;

        if ($newQty > 0) {
            Cart::add($item->id, $item->name, $item->price , $newQty, $item->attributes[0]);
        }

        $this->total = Cart::getTotal();
        $this->itemsQuantity = Cart::getTotalQuantity();
        $this->clearCash();
        // if ($this->itemsQuantity == 0) {
        // }

        $this->emit('scan-ok', 'Cantidad Actualizada');
    }

    public function clearCart()
    {
        Cart::clear();
        $this->clearCash();
        $this->total            = Cart::getTotal();
        $this->itemsQuantity    = Cart::getTotalQuantity();

        $this->emit('scan-ok', 'Carrito vacio');
    }

    public function saveSale()
    {
        // $this->emit('sale-ok', 'Venta registarda con exito!');
        // return;

        if ($this->total <= 0) {
            $this->emit('sale-error', 'AGREGA PRODUCTOS A LA VENTA');
            return;
        }
        if ($this->efectivo <= 0) {
            $this->emit('sale-error', 'INGRESA EL EFECTIVO');
            return;
        }
        if ($this->total > $this->efectivo) {
            $this->emit('sale-error', 'EL EFECTIVO DEBE SER MAYOR O IGUAL AL TOTAL');
            return;
        }

        DB::beginTransaction();

        try {
            $sale = Sale::create([
                'total'     => $this->total,
                'items'     => $this->itemsQuantity,
                'cash'      => $this->efectivo,
                'change'    => $this->change,
                'user_id'   =>Auth()->user()->id,
            ]);

            if ($sale) {
                $items = Cart::getContent();
                foreach($items as $item){
                    SaleDetail::create([
                        'price'         => $item->price,
                        'quantity'      => $item->quantity,
                        'product_id'    => $item->id,
                        'sale_id'       => $sale->id,
                    ]);
                    // update Stock
                    $product = Product::find($item->id);
                    $product->stock = $product->stock - $item->quantity;
                    $product->save();
                }
            }
            // Confirmamos transascion en la BD
            DB::commit();

            Cart::clear();
            $this->clearCash();
            $this->total            = Cart::getTotal();
            $this->itemsQuantity    = Cart::getTotalQuantity();

            $this->emit('sale-ok', 'Venta registarda con exito!');
            $this->emit('print-ticket', $sale->id);

        } catch (\Throwable $th) {
            // Borrar operaciones incompetas para mantener consistencia de la info
            DB::rollBack();
            $this->emit('sale-error', $th->getMessage());
        }
    }
    public function clearCash()
    {
        $this->efectivo = 0;
        $this->change   = 0;
        $this->emit('clearCash-ok', 'Limpia efectivo y cambio');
    }

    public function printTicket($sale)
    {
        return Redirect::to("print://$sale->id");
    }


}
