<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Spatie\Permission\Models\Permisisons;
use Spatie\Permission\Models\Role;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class RolesController extends Component
{
  use WithPagination;
  use WithFileUploads;

  public $selected_id, $componentName, $pageTitle, $search;
  public $name;

  private $pagination = 5;

  public function mount(){
    $this->componentName    = 'Roles';
    $this->pageTitle        = 'Listado';
    $this->selected_id      = 0;
  }

  public function paginationView()
  {
    return 'vendor.livewire.bootstrap';
  }
  public function render()
  {
    if (strlen($this->search) > 0) {
        $data = Role::where('name','like','%'.$this->search.'%')->paginate($this->pagination);
    }else{
        $data = Role::orderBy('name','asc')->paginate($this->pagination);
    }
    return view('livewire.roles.component',['data'=>$data])
        ->extends('layouts.theme.app')
        ->section('content');
  }

  public function store()
  {
    $rules = [
        'name'  => 'required|min:2|unique:roles,name'
    ];
    $messages = [
        'name.required' => 'El nombre del rol es requerido',
        'name.min'      => 'El nombre del rol debe tener al menos 2 caracteres',
        'name.unique'   => 'El rol ya exixte',
    ];
    $this->validate($rules, $messages);

    Role::create(['name' => $this->name]);

    $this->resetUI();
    $this->emit('item-added','Registro exitoso!');
  }


  public function edit($id)
  {
    $role = Role::find($id);
    if (isset($role)) {
        $this->selected_id  = $role->id;
        $this->name         = $role->name;
        $this->emit('item-modal-show', 'Mostar modal del Registro!');
        return;
    }else {
        $this->emit('item-error', 'No existe el registro!');
        return;
    }
  }

  public function update()
  {
    $rules = [
        'name'  => "required|min:2|unique:roles,name, {$this->selected_id}"
    ];
    $messages = [
        'name.required' => 'El nombre del rol es requerido',
        'name.min'      => 'El nombre del rol debe tener al menos 2 caracteres',
        'name.unique'   => 'El rol ya exixte',
    ];
    $this->validate($rules, $messages);

    $role = Role::find($this->selected_id);
    $role->name = $this->name;
    $role->save();

    $this->resetUI();
    $this->emit('item-updated','Registro Actualizado!');
  }

  protected $listeners = ['deleteRow' => 'destroy'];

  public function destroy($id)
  {
      /* Obtenemos permisos asignados al rol */
      $permissionsCount = Role::find($id)->permissions()->count();
      dd($permissionsCount);
      if ($permissionsCount > 0) {
          $this->emit('item-error','No se puede eliminar el rol porque tiene permisoso asignados');
          return;
      }

      Role::find($id)->delete();
      $this->resetUI();
      $this->emit('item-deleted','Registro eliminado con exito!');
  }

  public function resetUI(){
    $this->resetValidation();
    $this->resetPage();

    $this->selected_id  = 0;
    $this->name         = '';
    $this->search       = '';
  }
}
