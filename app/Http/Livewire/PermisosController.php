<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class PermisosController extends Component
{
  use WithPagination;
  use WithFileUploads;

  public $selected_id, $componentName, $pageTitle, $search;
  public $name;

  private $pagination = 10;

  public function mount(){
    $this->componentName    = 'Permisos';
    $this->pageTitle        = 'Listado';
    $this->selected_id      = 0;
  }

  public function paginationView()
  {
    return 'vendor.livewire.bootstrap';
  }
  public function render()
  {
    if (strlen($this->search) > 0) {
        $data = Permission::where('name','like','%'.$this->search.'%')->paginate($this->pagination);
    }else{
        $data = Permission::orderBy('id','asc')->paginate($this->pagination);
    }
    return view('livewire.permisos.component',['data'=>$data])
        ->extends('layouts.theme.app')
        ->section('content');
  }

  public function store()
  {
    $rules = [
        'name'  => 'required|min:5|unique:permissions,name'
    ];
    $messages = [
        'name.required' => 'El nombre del permiso es requerido',
        'name.min'      => 'El nombre del permiso debe tener al menos 5 caracteres',
        'name.unique'   => 'El permiso ya exixte',
    ];
    $this->validate($rules, $messages);

    Permission::create(['name' => $this->name]);

    $this->resetUI();
    $this->emit('item-added','Registro exitoso!');
  }


  public function edit($id)
  {
    $permiso = Permission::find($id);
    if (isset($permiso)) {
        $this->selected_id  = $permiso->id;
        $this->name         = $permiso->name;
        $this->emit('item-modal-show', 'Mostar modal del Registro!');
        return;
    }else {
        $this->emit('item-error', 'No existe el registro!');
        return;
    }
  }

  public function update()
  {
    $rules = [
        'name'  => "required|min:5|unique:permissions,name,{$this->selected_id}"
    ];
    $messages = [
        'name.required' => 'El nombre del permiso es requerido',
        'name.min'      => 'El nombre del permiso debe tener al menos 5 caracteres',
        'name.unique'   => 'El permiso ya exixte',
    ];
    $this->validate($rules, $messages);

    $permiso = Permission::find($this->selected_id);
    $permiso->name = $this->name;
    $permiso->save();

    $this->resetUI();
    $this->emit('item-updated','Registro Actualizado!');
  }

  protected $listeners = ['deleteRow' => 'destroy'];

  public function destroy($id)
  {
      /* Obtenemos roles asignados este permiso */
      $rolesCount = Permission::find($id)->getRoleNames()->count();
      if ($rolesCount > 0) {
          $this->emit('item-error','No se puede eliminar el permiso porque tiene roles asignados');
          return;
      }

      Permission::find($id)->delete();
      $this->resetUI();
      $this->emit('item-deleted','Registro eliminado con exito!');
  }

  public function resetUI(){
    $this->resetValidation();
    $this->resetPage();

    $this->selected_id  = 0;
    $this->name         = '';
    $this->search       = '';
  }
}
