<?php

namespace App\Http\Livewire;

use App\Models\Sale;
use App\Models\User;
use Carbon\Carbon;
use Livewire\Component;

class CashuotController extends Component
{
    public $from_date, $to_date, $user_id, $total, $items, $sales, $details;

    public function mount(){
        $this->from_date = null;
        $this->to_date = null;
        $this->user_id = 0;
        $this->total = 0;
        $this->sales = [];
        $this->details = [];
    }
    public function render()
    {
        return view('livewire.cashout.component', [
            'users' => User::orderBy('name','asc')->get()
            ])
            ->extends('layouts.theme.app')
            ->section('content');
    }

    public function consultar()
    {
        $fi = Carbon::parse($this->from_date)->format('Y-m-d') . ' 00:00:00';
        $ff = Carbon::parse($this->to_date)->format('Y-m-d') . ' 23:59:59';

        $this->sales = Sale::whereBetween('created_at',[$fi,$ff])
            ->where('status','PAID')
            ->where('user_id', $this->user_id)->get();

            $this->total = $this->sales ? $this->sales->sum('total') : 0 ;
            $this->items = $this->sales ? $this->sales->sum('items') : 0 ;

        if ($this->sales->count() <= 0) {
            $this->emit('consulta-error','No hay ventas en la consulta');
        }else {
            $this->emit('consulta-ok','Consulta exitosa!');
        }

    }


    public function viewDetails(Sale $sale)
    {
        $fi = Carbon::parse($this->from_date)->format('Y-m-d') . ' 00:00:00';
        $ff = Carbon::parse($this->to_date)->format('Y-m-d') . ' 23:59:59';

        $this->details = Sale::join('sale_details as d','d.sale_id','sales.id')
            ->join('products as p','p.id','d.product_id')
            ->select('d.sale_id','p.name as product','d.quantity','d.price')
            ->whereBetween('sales.created_at', [$fi,$ff])
            ->where('sales.status','PAID')
            ->where('sales.user_id', $this->user_id)
            ->where('sales.id', $sale->id)
            ->get();


        $this->emit('item-modal-show', 'Mostar vista details');
    }


    public function print()
    {
        # code...
    }
}
