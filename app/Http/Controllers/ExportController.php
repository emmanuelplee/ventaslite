<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function reportPDF($userId, $reportType, $from_date = null, $to_date = null)
    {
        $data = [];
        if ($reportType == 0) {
            $from = Carbon::parse(Carbon::now())->format('Y-m-d') . ' 00:00:00';
            $to = Carbon::parse(Carbon::now())->format('Y-m-d') . ' 23:59:59';
        }else {
            $from = Carbon::parse($from_date)->format('Y-m-d') . ' 00:00:00';
            $to = Carbon::parse($to_date)->format('Y-m-d') . ' 23:59:59';

            $from_date = Carbon::parse($from_date)->format('d-m-Y');
            $to_date = Carbon::parse($to_date)->format('d-m-Y');
        }

        if ($userId == 0) {
            $data = Sale::join('users as u','u.id','sales.user_id')
            ->select('sales.*','u.name as user')
            ->whereBetween('sales.created_at',[$from,$to])
            ->get();
        }else {
            $data = Sale::join('users as u','u.id','sales.user_id')
            ->select('sales.*','u.name as user')
            ->whereBetween('sales.created_at',[$from,$to])
            ->where('sales.user_id', $userId)
            ->get();
        }
        // dd($data);
        $suma = $data->sum(function($item){
            return ($item->total * $item->items);
        });
        $sumData = $suma;
        $countData = $data->sum('items');

        $user = $userId == 0 ? 'Todos' : User::find($userId)->name;

        $pdf = PDF::loadView('pdf.reporte', compact('data','reportType','user',
            'from_date','to_date','sumData','countData'));
        return $pdf->stream('salesReport.pdf');//visualizar
        // return $pdf->download('salesReport.pdf');//descargar
    }

}
